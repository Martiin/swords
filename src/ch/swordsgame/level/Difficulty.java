package ch.swordsgame.level;

public enum Difficulty {
	EASY(0),
	NORMAL(1),
	HARD(2),
	IMPOSSIBLE(3);
	
	public final int id;
	Difficulty(int id) {
		this.id = id;
	}
	
	public static Difficulty byId(int id) {
		for (Difficulty lt : Difficulty.values()) {
			if (lt.id == id) {
				return lt;
			}
		}
		return Difficulty.EASY;
	}
}