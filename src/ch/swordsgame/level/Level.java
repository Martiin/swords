package ch.swordsgame.level;

import java.awt.Graphics2D;
import java.util.Stack;

import ch.swordsgame.Swords;
import ch.swordsgame.level.room.Room;
import ch.swordsgame.level.room.Rooms;
import ch.swordsgame.util.Vector2DFloat;

/**
 * The Level object. It contains the rooms of the level.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class Level {
	/**
	 * The rooms of the level saved in a Stack.
	 */
	public Stack<Room> rooms; 
	
	public Level(int roomCount, Difficulty difficulty){
		rooms = new Stack<Room>();
		for (int i = 0; i < roomCount; i++) {
			Room room = Rooms.getRandomRoom(difficulty);
			rooms.push(room);
		}
	}

	/**
	 * Initializes the level.
	 */
	public void init() {
		rooms.peek().init();
	}

	/**
	 * Executes a tick on the current room and the player.
	 * 
	 * @param deltaTime The time which passed since the last tick.
	 */
	public void tick(double deltaTime) {
		if (!rooms.isEmpty()) {
			rooms.peek().tick(deltaTime);
		}
		Swords.player.tick(deltaTime);
	}

	/**
	 * Executes the render of the current room and the player.
	 * 
	 * @param g The graphics class which should be used to render.
	 */
	public void render(Graphics2D g) {
		if (!rooms.isEmpty()) {
			rooms.peek().render(g);
		}
	}
	
	/**
	 * Load next room.
	 */
	public void nextRoom() {
		rooms.pop();
		if (rooms.isEmpty()) {
			//TODO -> level cleared
		} else {
			rooms.peek().init();
		}
	}
}
