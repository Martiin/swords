package ch.swordsgame.level.room;

import java.awt.Graphics2D;

import ch.swordsgame.Assets;
import ch.swordsgame.Renderable;
import ch.swordsgame.util.Size;
import ch.swordsgame.util.Vector2DFloat;

public class WarpPoint extends Renderable {
	
	public WarpPoint(Vector2DFloat location) {
		super(45, location, new Size(32, 32));
	}
	
	@Override
	public void render(Graphics2D g) {
		Vector2DFloat pos = Vector2DFloat.convertWorldToScreen(location);
		Size ws = Size.convertWorldToScreen(new Size(32, 32));
		g.drawImage(Assets.teleporter, pos.intX(), pos.intY(), ws.width, ws.height, null);
	}
}
