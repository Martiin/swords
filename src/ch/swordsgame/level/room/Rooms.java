package ch.swordsgame.level.room;

import java.util.Random;
import java.util.Vector;

import ch.swordsgame.level.Difficulty;
import ch.swordsgame.util.ResourceLoader;
import ch.swordsgame.util.RoomLoader;

/**
 * The Rooms class. It contains all rooms and can return e specific or random room.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class Rooms {
	
	/**
	 * A Vector containing all rooms.
	 */
	public static Vector<Room> rooms;
	
	public Rooms() {
		rooms = new Vector<Room>();
	}
	
	/**
	 * Reads all rooms and adds them to the Vector rooms.
	 */
	public void init() {
		rooms.add(RoomLoader.read(ResourceLoader.GetDataInputStream("rooms/room01.room")));
		rooms.add(RoomLoader.read(ResourceLoader.GetDataInputStream("rooms/room02.room")));
	}
	
	/**
	 * 
	 * @param difficulty The difficulty determining which rooms should be returned.
	 * @return All rooms of the given difficulty.
	 */
	public static Vector<Room> getRoomsByDifficulty(Difficulty difficulty) {
		Vector<Room> rooms = new Vector<Room>();
		for (Room room : Rooms.rooms) {
			if (room.difficulty == difficulty) {
				rooms.add(room);
			}
		}
		return rooms;
	}
	
	/**
	 * Returns a random room of the given difficulty.
	 * @param difficulty The difficulty determining which rooms could be returned.
	 * @return A random room of the given difficulty.
	 */
	public static Room getRandomRoom(Difficulty difficulty) {
		Vector<Room> rooms = getRoomsByDifficulty(difficulty);
		int r = new Random().nextInt(rooms.size());
		return rooms.get(r).clone();
	}
}
