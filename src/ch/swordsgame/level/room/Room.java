package ch.swordsgame.level.room;

import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.util.Collections;
import java.util.Random;
import java.util.Vector;

import ch.swordsgame.Assets;
import ch.swordsgame.Renderable;
import ch.swordsgame.Swords;
import ch.swordsgame.entities.Enemy;
import ch.swordsgame.entities.EntityBase;
import ch.swordsgame.entities.enemies.EnemyManager;
import ch.swordsgame.entities.enemies.SpawnArea;
import ch.swordsgame.level.Difficulty;
import ch.swordsgame.level.blocks.Block;
import ch.swordsgame.level.tiles.Tile;
import ch.swordsgame.manager.HUDManager;
import ch.swordsgame.util.Size;
import ch.swordsgame.util.Vector2DFloat;

/**
 * The Room object. It contains its type, size, the spawnpoint of the player, the warp point, the blocks and the tiles.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class Room {
	
	/**
	 * The difficulty of the room.
	 */
	public Difficulty difficulty;
	
	/**
	 * The size of the room.
	 */
	public final Size size;
	
	/**
	 * The spawnpoint of the player.
	 */
	public Vector2DFloat spawnPoint;
	
	/**
	 * The warppoint of the room.
	 */
	public WarpPoint warpPoint;
	
	/**
	 * A Vector of all blocks.
	 */
	public Vector<Block> blocks;
	
	/**
	 * A Vector of all tiles.
	 */
	public Vector<Tile> tiles;
	
	/**
	 * A Vector containing all entities in the room.
	 */
	public Vector<EntityBase> entities;
	
	/**
	 * A Vector containing all spawn areas in the room.
	 */
	public Vector<SpawnArea> spawnAreas;
	
	/**
	 * Determines, how long it takes, until the next enemy is being spawned after the spawn of the pervious enemy.
	 */
	private int nextEnemySpawn;
	
	private Vector<Renderable> toRender;
	private Vector<Block> toRemove;
	
	private HUDManager hudManager;
	
	private EnemyManager enemyManager;
	
	private long spawnTickBlah = 0;
	
	/**
	 * The constructor. It initializes the variables of the room.
	 * 
	 * @param difficulty The difficulty of this room.
	 * @param size The size of this room.
	 */
	public Room(Difficulty difficulty, Size size){
		this.difficulty = difficulty;
		this.size = size;
		spawnPoint = new Vector2DFloat();
		warpPoint = new WarpPoint(new Vector2DFloat());
		blocks = new Vector<Block>();
		tiles = new Vector<Tile>();
		entities = new Vector<EntityBase>();
		toRender = new Vector<Renderable>();
		toRemove = new Vector<Block>();
		spawnAreas = new Vector<SpawnArea>();
		hudManager = new HUDManager();
		this.nextEnemySpawn = 0;
	}
	
	/**
	 * Initializes the room. (Player positioning etc.)
	 */
	public void init() {
		Swords.player.location = spawnPoint.copy();
		Size s = Size.convertWorldToScreen(new Size(size.width * 32, size.height * 32));
		Swords.camera.position = new Vector2DFloat();// = loc.invert();
		enemyManager = new EnemyManager(spawnAreas);
	}

	/**
	 * Executes a tick on each block in the blocks Vector.
	 * 
	 * @param deltaTime The time which passed since the last tick.
	 */
	public void tick(double deltaTime) {
		if ((System.currentTimeMillis() - spawnTickBlah) >= nextEnemySpawn) {
			Enemy enemy = enemyManager.spawnEnemy();
			if (enemy != null) {
				entities.add(enemy);
			}
			spawnTickBlah = System.currentTimeMillis();
			nextEnemySpawn = new Random().nextInt(2000);
		}
		toRender.clear();
		toRemove.clear();
		Rectangle renderArea = Swords.camera.getRenderArea();
		for (Tile t : tiles) {
			t.isActive = renderArea.intersects(t.getBounds());
			if (t.isActive)
				toRender.add(t);
		}
		warpPoint.isActive = renderArea.intersects(warpPoint.getBounds());
		if (warpPoint.isActive)
			toRender.add(warpPoint);
		Swords.player.isActive = renderArea.intersects(Swords.player.getBounds());
		if (Swords.player.isActive)
			toRender.add(Swords.player);
		for (Block b : blocks) {
			b.tick(deltaTime);
			b.isActive = renderArea.intersects(b.getBounds());
			if (b.toRemove)
				toRemove.add(b);
			else if (b.isActive)
				toRender.add(b);
		}
		for (EntityBase ent : entities) {
			ent.tick(deltaTime);
			ent.isActive = renderArea.intersects(ent.getBounds());
			if (ent.isActive)
				toRender.add(ent);
		}
		for (Block b : toRemove)
			blocks.remove(b);
		Collections.sort(toRender);
	}

	/**
	 * Executes the render on each tile of the tiles array and block of the blocks Vector.
	 * 
	 * @param g The graphics class which should be used to render the tile or block.
	 */
	public void render(Graphics2D g) {
		for (Renderable r : toRender) {
			r.render(g);
		}
		hudManager.render(g);
	}
	
	@Override
	public Room clone() {
		Room room = new Room(difficulty, size);
		room.blocks = (Vector<Block>) this.blocks.clone();
		room.tiles = (Vector<Tile>) this.tiles.clone();
		room.spawnPoint = this.spawnPoint.copy();
		room.spawnAreas = (Vector<SpawnArea>) this.spawnAreas.clone();
		room.warpPoint = new WarpPoint(warpPoint.location);
		return room;
	}
}
