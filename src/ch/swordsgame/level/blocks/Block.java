package ch.swordsgame.level.blocks;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;
import java.awt.Rectangle;
import java.util.Vector;

import ch.swordsgame.Renderable;
import ch.swordsgame.Swords;
import ch.swordsgame.collision.CollisionCheck;
import ch.swordsgame.manager.DebugManager;
import ch.swordsgame.util.Size;
import ch.swordsgame.util.Vector2DFloat;

/**
 * The Block object.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class Block extends Renderable {
	/**
	 * The image of the block.
	 */
	public Image image;
	
	/**
	 * The colliders of the block.
	 */
	protected Vector<Rectangle> colliders;
	
	/**
	 * The type of the block.
	 */
	public BlockType type;
	
	/**
	 * Determines whether the block is solid or not. Defaults to true.
	 */
	public boolean isSolid;
	
	/**
	 * Determines whether the block must been removed from the list of the blocks in the current room or not.
	 */
	public boolean toRemove;
	
	/**
	 * The constructor.
	 * 
	 * @param type Defines what kind of block it is.
	 * @param image The image of this block.
	 * @param size The size of the block.
	 */
	public Block(BlockType type, Image image, Size size) {
		super(50, new Vector2DFloat(), size);
		this.image = image;
		this.type = type;
		this.isSolid = true;
		this.isActive = false;
		this.colliders = new Vector<Rectangle>();
		this.colliders.add(new Rectangle(0, 0, size.width, size.height));
	}
	
	/**
	 * Can be attached to the constructor call.
	 * @param solid Sets the Block solid or not.
	 * @return The Block with the new isSolid value.
	 */
	public Block isSolid(boolean solid) {
		isSolid = solid;
		return this;
	}
	
	/**
	 * Can be attached to the constructor call. Sets the collider to the block.
	 * @param x The colliders x location relative to the location of the block.
	 * @param y The colliders y location relative to the location of the block.
	 * @param width The colliders width.
	 * @param height The colliders height.
	 * @return The Block with the new collider.
	 */
	public Block collider(int x, int y, int width, int height) {
		colliders.clear();
		colliders.add(new Rectangle(x, y, width, height));
		return this;
	}
	
	/**
	 * Can be attached to the constructor call. Adds a collider to the block.
	 * @param x The colliders x location relative to the location of the block.
	 * @param y The colliders y location relative to the location of the block.
	 * @param width The colliders width.
	 * @param height The colliders height.
	 * @return The Block with the new collider.
	 */
	public Block addCollider(int x, int y, int width, int height) {
		colliders.add(new Rectangle(x, y, width, height));
		return this;
	}
	
	/**
	 * Can be attached to the constructor call. Used to create a block without collider.
	 * @return The Block with the new collider.
	 */
	public Block noCollider() {
		colliders.clear();
		return this;
	}

	/**
	 * Executes a tick.
	 * 
	 * @param deltaTime The time which passed since the last tick.
	 */
	public void tick(double deltaTime) {
		if (type == BlockType.DIAMOND && Swords.player.getCollider().intersects(this.getColliders().firstElement())){
			Swords.player.diamonds += 1;
			this.toRemove = true;
		}
	}
	
	/**
	 * The box collider at the current in-world position
	 * @return A Rectangle object representing the box collider.
	 */
	public Vector<Rectangle> getColliders() {
		Vector<Rectangle> cols = new Vector<Rectangle>();
		for (Rectangle col : colliders) {
			Rectangle c = (Rectangle) col.clone();
			c.x += location.x;
			c.y += location.y;
			cols.add(c);
		}
		return cols;
	}

	/**
	 * Renders the block.
	 * 
	 * @param g The graphics class which should be used to render the tile or block.
	 */
	public void render(Graphics2D g) {
		Vector2DFloat loc = Vector2DFloat.convertWorldToScreen(location);
		Size s = Size.convertWorldToScreen(size);
		g.drawImage(image, loc.intX(), loc.intY(), s.width, s.height, null);
		if (DebugManager.BLOCKS == 1 || DebugManager.BLOCKS == 3) {
			g.setColor(Color.BLUE);
			for (Rectangle collider : getColliders()) {
				Size cs = Size.convertWorldToScreen(new Size(collider.width, collider.height));
				Vector2DFloat cloc = Vector2DFloat.convertWorldToScreen(new Vector2DFloat(collider.x, collider.y));
				g.drawRect(cloc.intX(), cloc.intY(), cs.width, cs.height);
			}
		}
		if (DebugManager.BLOCKS == 2 || DebugManager.BLOCKS == 3) {
			int size1 = (int) (4 * Swords.camera.zoom);
			int size2 = (int) (5 * Swords.camera.zoom);
			int add = 2 * Swords.camera.zoom < 2 ? 0 : 2;
			int now = add;
			g.setColor(new Color(20, 20, 20, 192));
			g.fillRect(loc.intX(), loc.intY(), s.width, s.height);
			g.setStroke(new BasicStroke(1));
			g.setFont(new Font("Consolas", Font.PLAIN, size1));
			g.setColor(Color.CYAN);
			g.drawString(type.name(), loc.x + 2, loc.y + (now += size1 + add));
			g.setFont(new Font("Consolas", Font.PLAIN, size2));
			g.setColor(Color.PINK);
			g.drawString((int) (location.x / 32) + "," + (int) (location.y / 32), loc.x + 2, loc.y + (now += size2 + add));
			g.setColor(Color.MAGENTA);
			g.drawString(location.x + ", " + location.x, loc.x + 2, loc.y + (now += size2 + add));
			g.setColor(Color.YELLOW);
			g.drawString(loc.y + ", " + loc.x, loc.x + 2, loc.y + (now += size2 + add));
		}
	}
}
