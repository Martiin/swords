package ch.swordsgame.level.blocks;

import ch.swordsgame.Assets;
import ch.swordsgame.util.Size;

/**
 * The BlockType defines what kind of block a block is.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public enum BlockType {
	DIAMOND(1),
	WALL_FRONT1(2),
	WALL_FRONT_LEFT(3),
	WALL_FRONT_RIGHT(4),
	WALL_FRONT_TOP(5),
	WALL_FRONT_TOP_LEFT(6),
	WALL_FRONT_TOP_RIGHT(7),
	WALL_LEFT_UP(8),
	WALL_RIGHT_UP(9),
	WALL_TOP(10),
	WALL_TOP_LEFT_UP(11),
	WALL_TOP_RIGHT_UP(12),
	WALL_TOP_FULL(13),
	WALL_CORNER_FRONT_LEFT(14),
	WALL_CORNER_FRONT_RIGHT(15),
	WALL_CORNER_FRONT_TOP_LEFT(16),
	WALL_CORNER_FRONT_TOP_RIGTH(17),
	WALL_LEFT_DOWN(18),
	WALL_RIGHT_DOWN(19),
	WALL_TOP_LEFT_DOWN(20),
	WALL_TOP_RIGHT_DOWN(21),
	
	WALL_CORNER_RIGHT_BACK_UP(22),
	WALL_CORNER_RIGHT_BACK_DOWN(23),
	WALL_CORNER_RIGHT_FRONT_UP(24),
	WALL_CORNER_RIGHT_FRONT_DOWN(25),
	WALL_CORNER_LEFT_BACK_UP(26),
	WALL_CORNER_LEFT_BACK_DOWN(27),
	WALL_CORNER_LEFT_FRONT_UP(28),
	WALL_CORNER_LEFT_FRONT_DOWN(29),
	WALL_BACK(30),
	WALL_FRONT(31),
	WALL_CORNER_WALL_LEFT(32),
	WALL_CORNER_WALL_RIGHT(33),
	UNKNOWN(0);
	
	/**
	 * The id of the BlockType.
	 */
	public final int id;
	
	private BlockType(int id) {
		this.id = id;
	}
	
	/**
	 * Creates a new block object with the type of the enum.
	 * 
	 * @return A block object.
	 */
	public Block getBlock() {
		switch (this) {
			case DIAMOND:
				return new Block(this, Assets.diamond, new Size(16, 16)).isSolid(false);
			case WALL_FRONT1:
				return new Block(this, Assets.wall_front1, new Size(32, 32)).collider(0, 20, 32, 12);
			case WALL_FRONT_LEFT:
				return new Block(this, Assets.wall_front_left, new Size(32, 32));
			case WALL_FRONT_RIGHT:
				return new Block(this, Assets.wall_front_right, new Size(32, 32));
			case WALL_FRONT_TOP:
				return new Block(this, Assets.wall_front_top, new Size(32, 32));
			case WALL_FRONT_TOP_LEFT:
				return new Block(this, Assets.wall_front_top_left, new Size(32, 32));
			case WALL_FRONT_TOP_RIGHT:
				return new Block(this, Assets.wall_front_top_right, new Size(32, 32));
			case WALL_LEFT_UP:
				return new Block(this, Assets.wall_left, new Size(32, 32)).collider(0, 0, 12, 32);
			case WALL_RIGHT_UP:
				return new Block(this, Assets.wall_right, new Size(32, 32)).collider(20, 0, 12, 32);
			case WALL_LEFT_DOWN:
				return new Block(this, Assets.wall_left, new Size(32, 32)).noCollider();
			case WALL_RIGHT_DOWN:
				return new Block(this, Assets.wall_right, new Size(32, 32)).noCollider();
			case WALL_TOP:
				return new Block(this, Assets.wall_top, new Size(32, 32)).noCollider();
			case WALL_TOP_LEFT_UP:
				return new Block(this, Assets.wall_top_left, new Size(32, 32)).collider(0, 0, 12, 32);
			case WALL_TOP_RIGHT_UP:
				return new Block(this, Assets.wall_top_right, new Size(32, 32)).collider(20, 0, 12, 32);
			case WALL_TOP_LEFT_DOWN:
				return new Block(this, Assets.wall_top_left, new Size(32, 32)).noCollider();
			case WALL_TOP_RIGHT_DOWN:
				return new Block(this, Assets.wall_top_right, new Size(32, 32)).noCollider();
			case WALL_TOP_FULL:
				return new Block(this, Assets.wall_top_full, new Size(32, 32));
			case WALL_CORNER_FRONT_LEFT:
				return new Block(this, Assets.wall_corner_front_left, new Size(32, 32)).collider(0, 0, 12, 32);
			case WALL_CORNER_FRONT_RIGHT:
				return new Block(this, Assets.wall_corner_front_right, new Size(32, 32)).collider(20, 0, 12, 32);
			case WALL_CORNER_FRONT_TOP_LEFT:
				return new Block(this, Assets.wall_corner_front_top_left, new Size(32, 32)).collider(0, 0, 12, 32);
			case WALL_CORNER_FRONT_TOP_RIGTH:
				return new Block(this, Assets.wall_corner_front_top_right, new Size(32, 32)).collider(20, 0, 12, 32);
			
			case WALL_CORNER_LEFT_BACK_UP:
				return new Block(this, Assets.wall_corner_left_back, new Size(32, 64)).collider(0, 0, 32, 64);
			case WALL_CORNER_LEFT_BACK_DOWN:
				return new Block(this, Assets.wall_corner_left_back, new Size(32, 64)).collider(0, 52, 32, 12);
			case WALL_CORNER_LEFT_FRONT_UP:
				return new Block(this, Assets.wall_corner_left_front, new Size(32, 64)).collider(0, 52, 32, 12);
			case WALL_CORNER_LEFT_FRONT_DOWN:
				return new Block(this, Assets.wall_corner_left_front, new Size(32, 64)).collider(0, 0, 12, 64).addCollider(0, 52, 32, 12);
			case WALL_CORNER_RIGHT_BACK_UP:
				return new Block(this, Assets.wall_corner_right_back, new Size(32, 64)).collider(0, 0, 32, 64);
			case WALL_CORNER_RIGHT_BACK_DOWN:
				return new Block(this, Assets.wall_corner_right_back, new Size(32, 64)).collider(0, 52, 32, 12);
			case WALL_CORNER_RIGHT_FRONT_UP:
				return new Block(this, Assets.wall_corner_right_front, new Size(32, 64)).collider(0, 52, 32, 12);
			case WALL_CORNER_RIGHT_FRONT_DOWN:
				return new Block(this, Assets.wall_corner_right_front, new Size(32, 64)).collider(20, 0, 12, 64).addCollider(0, 52, 32, 12);
			case WALL_BACK:
				return new Block(this, Assets.wall_back, new Size(32, 64));
			case WALL_FRONT:
				return new Block(this, Assets.wall_front, new Size(32, 64)).collider(0, 52, 32, 12);
			case WALL_CORNER_WALL_LEFT:
				return new Block(this, Assets.wall_corner_wall_left, new Size(32, 64)).collider(20, 52, 12, 12);
			case WALL_CORNER_WALL_RIGHT:
				return new Block(this, Assets.wall_corner_wall_right, new Size(32, 64)).collider(0, 52, 12, 12);
			case UNKNOWN:
			default:
			return new Block(this, null, new Size(32, 32));
		}
	}
	
	/**
	 * Returns a BlockType of the given id.
	 * 
	 * @param id The id which should match the BlockType.
	 * @return A BlockType matching the given id.
	 */
	public static BlockType byId(int id) {
		for (BlockType bt : BlockType.values()) {
			if (bt.id == id) {
				return bt;
			}
		}
		return BlockType.UNKNOWN;
	}
}