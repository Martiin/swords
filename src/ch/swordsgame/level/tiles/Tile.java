package ch.swordsgame.level.tiles;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Image;

import ch.swordsgame.Renderable;
import ch.swordsgame.Swords;
import ch.swordsgame.manager.DebugManager;
import ch.swordsgame.util.Size;
import ch.swordsgame.util.Vector2DFloat;

/**
 * The Tile object. All objects which are the ground are tiles. They can't collide, cause they are the floor.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class Tile extends Renderable {
	
	/**
	 * The image of the Tile, which will be rendered on the screen.
	 */
	public Image image;
	
	/**
	 * The type determines what kind of Tile this Tile is.
	 */
	public TileType type;
	
	/**
	 * The constructor. The default value of active is set false.
	 * 
	 * @param type The type, that the Tile should have.
	 * @param image The image of the Tile.
	 * @param size The size of the Tile.
	 */
	public Tile(TileType type, Image image, Size size) {
		super(-10, new Vector2DFloat(), size);
		this.image = image;
		this.type = type;
	}

	/**
	 * Executes the render on this tile.
	 * 
	 * @param g The graphics class which should be used to render the tile or block.
	 */
	public void render(Graphics2D g) {
		Vector2DFloat loc = Vector2DFloat.convertWorldToScreen(location);
		Size s = Size.convertWorldToScreen(size);
		g.drawImage(image, loc.intX(), loc.intY(), s.width, s.height, null);
		if (DebugManager.TILES == 1 || DebugManager.TILES == 3) {
			g.setColor(Color.WHITE);
			g.drawRect(loc.intX() * 32, loc.intY() * 32, s.width, s.height);
		}
		if (DebugManager.TILES == 2 || DebugManager.TILES == 3) {
			int size1 = (int) (4 * Swords.camera.zoom);
			int size2 = (int) (5 * Swords.camera.zoom);
			int add = 2 * Swords.camera.zoom < 2 ? 0 : 2;
			int now = add;
			int gX = loc.intX() * 32;
			int gY = loc.intY() * 32;
			g.setStroke(new BasicStroke(1));
			g.setFont(new Font("Consolas", Font.PLAIN, size1));
			g.setColor(Color.CYAN);
			g.drawString(type.name(), gX + 2, gY + (now += size1 + add));
			g.setFont(new Font("Consolas", Font.PLAIN, size2));
			g.setColor(Color.PINK);
			g.drawString(location.x + "," + location.y, gX + 2, gY + (now += size2 + add));
			g.setColor(Color.MAGENTA);
			g.drawString((loc.x * 32) + ", " + (loc.y * 32), gX + 2, gY + (now += size2 + add));
			g.setColor(Color.YELLOW);
			g.drawString(gX + ", " + gY, gX + 2, gY + (now += size2 + add));
		}
	}
}
