package ch.swordsgame.level.tiles;

import java.awt.image.BufferedImage;
import java.util.Random;

import ch.swordsgame.Assets;
import ch.swordsgame.util.Size;

/**
 * The TileType determines what kind of Tile a Tile is.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public enum TileType {
	// must start with 1!!!
	DIRT_RANDOM(1),
	DIRT_1(2),
	DIRT_2(3),
	DIRT_3(4),
	DIRT_4(5),
	DIRT_5(6),
	DIRT_6(7),
	DIRT_7(8),
	DIRT_TOP(9),
	DIRT_BOTTOM(10),
	DIRT_LEFT(11),
	DIRT_RIGHT(12),
	DIRT_TOP_LEFT(13),
	DIRT_TOP_RIGHT(14),
	DIRT_BOTTOM_LEFT(15),
	DIRT_BOTTOM_RIGHT(16),
	STONE_1(17);
	
	/**
	 * ????????????????????????????????????????????????????????????????????????
	 */
	public final int id;
	private TileType(int id) {
		this.id = id;
	}
	
	/**
	 * Returns a Tile matching the current enum.
	 * 
	 * @return A Tile matching this enum.
	 */
	public Tile getTile() {
		switch (this) {
		case DIRT_RANDOM:
			BufferedImage img = null;
			int r = new Random().nextInt(7);
			switch (r) {
			case 0:
				img = Assets.dirt_1;
				break;
			case 1:
				img = Assets.dirt_2;
				break;
			case 2:
				img = Assets.dirt_3;
				break;
			case 3:
				img = Assets.dirt_4;
				break;
			case 4:
				img = Assets.dirt_5;
				break;
			case 5:
				img = Assets.dirt_6;
				break;
			case 6:
				img = Assets.dirt_7;
				break;
			}
			return new Tile(this, img, new Size(32, 32));
		case DIRT_1:
			return new Tile(this, Assets.dirt_1, new Size(32, 32));
		case DIRT_2:
			return new Tile(this, Assets.dirt_2, new Size(32, 32));
		case DIRT_3:
			return new Tile(this, Assets.dirt_3, new Size(32, 32));
		case DIRT_4:
			return new Tile(this, Assets.dirt_4, new Size(32, 32));
		case DIRT_5:
			return new Tile(this, Assets.dirt_5, new Size(32, 32));
		case DIRT_6:
			return new Tile(this, Assets.dirt_6, new Size(32, 32));
		case DIRT_7:
			return new Tile(this, Assets.dirt_7, new Size(32, 32));
		case DIRT_TOP:
			return new Tile(this, Assets.dirt_top, new Size(32, 32));
		case DIRT_BOTTOM:
			return new Tile(this, Assets.dirt_bottom, new Size(32, 32));
		case DIRT_LEFT:
			return new Tile(this, Assets.dirt_left, new Size(32, 32));
		case DIRT_RIGHT:
			return new Tile(this, Assets.dirt_right, new Size(32, 32));
		case DIRT_TOP_LEFT:
			return new Tile(this, Assets.dirt_top_left, new Size(32, 32));
		case DIRT_TOP_RIGHT:
			return new Tile(this, Assets.dirt_top_right, new Size(32, 32));
		case DIRT_BOTTOM_LEFT:
			return new Tile(this, Assets.dirt_bottom_left, new Size(32, 32));
		case DIRT_BOTTOM_RIGHT:
			return new Tile(this, Assets.dirt_bottom_right, new Size(32, 32));
		case STONE_1:
			return new Tile(this, Assets.stone_1, new Size(32, 32));
		default:
			return null;
		}
	}
	
	/**
	 * Returns a TileType of the given id.
	 * 
	 * @param id The id which should match the TileType.
	 * @return A TileType matching the given id.
	 */
	public static TileType byId(int id) {
		for (TileType tt : TileType.values()) {
			if (tt.id == id) {
				return tt;
			}
		}
		return null;
	}
}