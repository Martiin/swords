package ch.swordsgame;

import java.awt.image.BufferedImage;

import ch.swordsgame.SpriteSheet;
import ch.swordsgame.util.ResourceLoader;

/**
 * The class which holds all assets.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class Assets {
	
	/**
	 * The spritesheet for the player, containing all its frames.
	 */
	public static SpriteSheet player = new SpriteSheet();
	/**
	 * The spritesheet for the slime, containing all its frames.
	 */
	public static SpriteSheet slime = new SpriteSheet();
	/**
	 * The spritesheet for the bat, containing all its frames.
	 */
	public static SpriteSheet bat = new SpriteSheet();
	/**
	 * The spritesheet for the tiles, containing all its frames.
	 */
	public static SpriteSheet tiles = new SpriteSheet();
	/**
	 * The spritesheet for the blocks, containing all its frames.
	 */
	public static SpriteSheet blocks = new SpriteSheet();
	/**
	 * The spritesheet for the icons, containing all its frames.
	 */
	public static SpriteSheet icons = new SpriteSheet();
	
	/**
	 * A dirt image.
	 */
	public static BufferedImage
		stone_1,
		dirt_1,
		dirt_2,
		dirt_3,
		dirt_4,
		dirt_5,
		dirt_6,
		dirt_7,
		dirt_top,
		dirt_bottom,
		dirt_left,
		dirt_right,
		dirt_top_left,
		dirt_top_right,
		dirt_bottom_left,
		dirt_bottom_right;
	
	/**
	 * The diamond image.
	 */
	public static BufferedImage diamond;

	/**
	 * The diamond image.
	 */
	public static BufferedImage teleporter;
	
	/**
	 * A wall image.
	 */
	public static BufferedImage
		wall_front1,
		wall_front_left,
		wall_front_right,
		wall_front_top,
		wall_front_top_left,
		wall_front_top_right,
		wall_left,
		wall_right,
		wall_top,
		wall_top_left,
		wall_top_right,
		wall_top_full,
		wall_corner_front_left,
		wall_corner_front_right,
		wall_corner_front_top_left,
		wall_corner_front_top_right,

		wall_corner_left_back,
		wall_corner_left_front,
		wall_corner_right_back,
		wall_corner_right_front,
		wall_back,
		wall_front,
		wall_corner_wall_left,
		wall_corner_wall_right;
	
	/**
	 * The initialization of all the images and spritesheets.
	 */
	public void init() {
		//spritesheets
		player.setSpriteSheet(ResourceLoader.LoadImage("textures/entities/player.png"), 50, 40);
		slime.setSpriteSheet(ResourceLoader.LoadImage("textures/entities/slime.png"), 32, 32);
		bat.setSpriteSheet(ResourceLoader.LoadImage("textures/entities/bat.png"), 32, 32);
		tiles.setSpriteSheet(ResourceLoader.LoadImage("textures/tiles.png"), 32, 32);
		blocks.setSpriteSheet(ResourceLoader.LoadImage("textures/blocks.png"), 32, 32);
		
		//other things
		teleporter = blocks.getTile(3, 1);
		
		//tiles
		stone_1 = tiles.getTile(0, 3);
		dirt_1 = tiles.getTile(0, 0);
		dirt_2 = tiles.getTile(4, 1);
		dirt_3 = tiles.getTile(0, 2);
		dirt_4 = tiles.getTile(1, 2);
		dirt_5 = tiles.getTile(2, 2);
		dirt_6 = tiles.getTile(3, 2);
		dirt_7 = tiles.getTile(4, 2);
		dirt_top = tiles.getTile(1, 0);
		dirt_bottom = tiles.getTile(2, 0);
		dirt_left = tiles.getTile(3, 0);
		dirt_right = tiles.getTile(4, 0);
		dirt_top_left = tiles.getTile(0, 1);
		dirt_top_right = tiles.getTile(1, 1);
		dirt_bottom_left = tiles.getTile(2, 1);
		dirt_bottom_right = tiles.getTile(3, 1);

		//blocks
		diamond = blocks.getTile(1, 2);
		wall_front = blocks.getTile(1, 1);
		wall_front_left = blocks.getTile(0, 1);
		wall_front_right = blocks.getTile(2, 1);
		wall_front_top = blocks.getTile(1, 0);
		wall_front_top_left = blocks.getTile(0, 0);
		wall_front_top_right = blocks.getTile(2, 0);
		wall_left = blocks.getTile(0, 2);
		wall_right = blocks.getTile(2, 2);
		wall_top = blocks.getTile(1, 3);
		wall_top_left = blocks.getTile(0, 3);
		wall_top_right = blocks.getTile(2, 3);
		wall_top_full = blocks.getTile(3, 0);
		wall_corner_front_left = blocks.getTile(4, 0);
		wall_corner_front_right = blocks.getTile(4, 1);
		wall_corner_front_top_left = blocks.getTile(3, 3);
		wall_corner_front_top_right = blocks.getTile(3, 2);

		wall_corner_left_back = blocks.getDoubeTile(0, 0, 0, 1);
		wall_corner_left_front = blocks.getDoubeTile(0, 3, 1, 1);
		wall_corner_right_back = blocks.getDoubeTile(2, 0, 2, 1);
		wall_corner_right_front = blocks.getDoubeTile(2, 3, 1, 1);
		wall_back = blocks.getDoubeTile(1, 0, 1, 1);
		wall_front = blocks.getDoubeTile(1, 3, 1, 1);
		wall_corner_wall_left = blocks.getDoubeTile(2, 2, 2, 2);
		wall_corner_wall_right = blocks.getDoubeTile(0, 2, 0, 2);
	}
}
