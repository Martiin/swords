package ch.swordsgame.util;

import java.awt.image.BufferedImage;
import java.io.DataInputStream;
import java.io.IOException;
import java.net.URL;

import javax.imageio.ImageIO;

import ch.swordsgame.Main;

/**
 * Loads resources, like images.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class ResourceLoader {

	/**
	 * Loads an image from the specified path.
	 * 
	 * @param path The path where the image is saved.
	 * @return Returns the image from the specified path.
	 */
	public static BufferedImage LoadImage(String path) {
		URL url = Main.class.getResource("/" + path);
		BufferedImage img = null;
		try {
			img = ImageIO.read(url);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return img;
	}

	/**
	 * Creates and returns a DataInputStream.
	 * 
	 * @param path The path for the stream.
	 * @return The DataInputStream with the given path.
	 */
	public static DataInputStream GetDataInputStream(String path) {
		return new DataInputStream(Main.class.getResourceAsStream("/" + path));
	}
}
