package ch.swordsgame.util;

import java.awt.Point;

import ch.swordsgame.Swords;

/**
 * A location using floating points.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class Vector2DFloat {
	
	/**
	 * The x coordinate.
	 */
	public float x;
	
	/**
	 * The y coordinate.
	 */
	public float y;
	
	/**
	 * The constructor. It initializes x and y to 0.
	 */
	public Vector2DFloat() {
		this.x = 0;
		this.y = 0;
	}
	
	/**
	 * A constructor with the x and y coordinates as parameters.
	 * 
	 * @param x The x coordinate.
	 * @param y The y coordinate.
	 */
	public Vector2DFloat(float x, float y) {
		this.x = x;
		this.y = y;
	}

	/**
	 * Compares the given object with this object.
	 * 
	 * @param obj The object to compare with.
	 */
	public boolean equals(Object obj) {
		if (obj instanceof Vector2DFloat) {
			Vector2DFloat v = (Vector2DFloat) obj;
			return this.x == v.x && this.y == v.y;
		}
		return false;
	}
	
	/**
	 * Copies this object.
	 * 
	 * @return
	 */
	public Vector2DFloat copy() {
		return new Vector2DFloat(this.x, this.y);
	}
	
	/**
	 * Adds the given vector to this vector.
	 * 
	 * @param vec The vector which should be added to this vector.
	 * @return The new Vector.
	 */
	public Vector2DFloat add(Vector2DFloat vec) {
		x += vec.x;
		y += vec.y;
		return new Vector2DFloat(x, y);
	}
	
	/**
	 * Subtracts the given vector from this vector.
	 * 
	 * @param vec The vector from which should be subtracted.
	 * @return The new Vector.
	 */
	public Vector2DFloat subtract(Vector2DFloat vec) {
		x -= vec.x;
		y -= vec.y;
		return new Vector2DFloat(x, y);
	}
	
	/**
	 * Multiplies the given double with this vector.
	 * 
	 * @param d The double to multiply this vector with.
	 * @return The new Vector.
	 */
	public Vector2DFloat multiply(double d) {
		x = (int) (x * d);
		y = (int) (y * d);
		return new Vector2DFloat(x, y);
	}
	
	/**
	 * Divides this vector through the given double.
	 * 
	 * @param d The double to divide this vector through.
	 * @return The new vector.
	 */
	public Vector2DFloat divide(double d) {
		x = (int) (x / d);
		y = (int) (y / d);
		return new Vector2DFloat(x, y);
	}
	
	/**
	 * Inverts this vector.
	 * 
	 * @return The new vector.
	 */
	public Vector2DFloat invert() {
		return new Vector2DFloat(-x, -y);
	}
	
	/**
	 * Returns the x coordinate in form of an int.
	 * 
	 * @return The x coordinate in form of an int.
	 */
	public int intX() {
		return (int) Math.round(x);
	}

	/**
	 * Returns the y coordinate in form of an int.
	 * 
	 * @return The y coordinate in form of an int.
	 */
	public int intY() {
		return (int) Math.round(y);
	}
	
	/**
	 * Converts this vector to a Point.
	 * 
	 * @return The point converted from this vector.
	 */
	public Point toPoint() {
		return new Point((int) Math.round(x), (int) Math.round(y));
	}
	
	/**
	 * Converts the given vector from the screen to the world, depending on the location and zoom of the camera.
	 * @param pos The location.
	 * @return The new vector.
	 */
	public static Vector2DFloat convertScreenToWorld(Vector2DFloat pos) {
		Vector2DFloat offset = Swords.camera.position;
		Vector2DFloat converted = pos.copy().add(offset);
		converted.divide(Swords.camera.zoom);
		return converted;
	}
	
	/**
	 * Converts the given vector from the world to the screen, depending on the location and zoom of the camera.
	 * @param pos The location.
	 * @return The new vector.
	 */
	public static Vector2DFloat convertWorldToScreen(Vector2DFloat pos) {
		Vector2DFloat converted = pos.copy().multiply(Swords.camera.zoom);
		Vector2DFloat offset = Swords.camera.position;
		return converted.subtract(offset);
	}
}
