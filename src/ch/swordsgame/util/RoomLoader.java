package ch.swordsgame.util;

import java.awt.Rectangle;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import ch.swordsgame.entities.EntityType;
import ch.swordsgame.entities.enemies.SpawnArea;
import ch.swordsgame.level.Difficulty;
import ch.swordsgame.level.blocks.Block;
import ch.swordsgame.level.blocks.BlockType;
import ch.swordsgame.level.room.Room;
import ch.swordsgame.level.tiles.Tile;
import ch.swordsgame.level.tiles.TileType;

/**
 * Reads a room file.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class RoomLoader {
	private static final int VERSION = 3;
	
	/**
	 * Returns a room read from the given DataInputStream.
	 * @param input The DataInputStream which should be used to read the file.
	 * @return The room which was read.
	 */
	public static Room read(DataInputStream input) {
		Room room = null;
		try {
			//room
			int version = input.readInt();
			Difficulty difficulty = Difficulty.byId(input.readInt());
			int rWidth = input.readInt();
			int rHeight = input.readInt();
			room = new Room(difficulty, new Size(rWidth, rHeight));
			//spawnpoint
			room.spawnPoint.x = input.readInt();
			room.spawnPoint.y = input.readInt();
			//warppoint
			room.warpPoint.location.x = input.readInt();
			room.warpPoint.location.y = input.readInt();
			//tiles v1
			if (version == 1) {
				for (int x = 0; x < room.size.width; x++) {
					for (int y = 0; y < room.size.height; y++) {
						int id = input.readInt();
						Tile tile = id > 0 ? TileType.byId(id).getTile() : null;
						if (tile != null) {
							tile.location = new Vector2DFloat(x * 32, y * 32);
							room.tiles.add(tile);
						}
					}
				}
			}
			//blocks
			int count = input.readInt();
			for (int i = 0; i < count; i++) {
				Block block = BlockType.byId(input.readInt()).getBlock();
				int bX = input.readInt();
				int bY = input.readInt();
				block.location = new Vector2DFloat(bX, bY);
				if (version == 1) {
					input.readInt();
					input.readInt();
				}
				int dataLenght = input.readInt();
				if (dataLenght > 0) {
					//TODO
				}
				room.blocks.add(block);
			}
			//ab version 2
			if (version > 1) {
				//tiles v2
				count = input.readInt();
				for (int i = 0; i < count; i++) {
					Tile tile = TileType.byId(input.readInt()).getTile();
					int tX = input.readInt();
					int tY = input.readInt();
					tile.location = new Vector2DFloat(tX, tY);
					room.tiles.add(tile);
				}
			}
			//ab version 3
			if (version > 2) {
				//spawnareas
				count = input.readInt();
				for (int i = 0; i < count; i++) {
					SpawnArea spawnarea = new SpawnArea();
					int sX = input.readInt();
					int sY = input.readInt();
					int sWidth = input.readInt();
					int sHeight = input.readInt();
					spawnarea.area = new Rectangle(sX, sY, sWidth, sHeight);
					spawnarea.maxEnemies = input.readInt();
					spawnarea.maxSpawns = input.readInt();
					spawnarea.entityType = EntityType.byId(input.readInt());
					room.spawnAreas.add(spawnarea);
				}
			}
			input.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return room;
	}
	
	/**
	 * Saves a room to the filesystem.
	 * 
	 * @param room The room which should be saved.
	 * @param output The DataOutputStream which should be used to save the room.
	 */
	public static void save(Room room, DataOutputStream output) {
		try {
			//room
			output.writeInt(VERSION);
			output.writeInt(room.difficulty.id);
			output.writeInt(room.size.width);
			output.writeInt(room.size.height);
			//spawnpoint
			output.writeInt(room.spawnPoint.intX());
			output.writeInt(room.spawnPoint.intY());
			//warppoint
			output.writeInt(room.warpPoint.location.intX());
			output.writeInt(room.warpPoint.location.intY());
			//blocks
			output.writeInt(room.blocks.size());
			for (Block block : room.blocks) {
				output.writeInt(block.type.id);
				output.writeInt(block.location.intX());
				output.writeInt(block.location.intY());
				output.writeInt(0); //dataLength
			}
			//tiles
			output.writeInt(room.tiles.size());
			for (Tile tile : room.tiles) {
				output.writeInt(tile.type.id);
				output.writeInt(tile.location.intX());
				output.writeInt(tile.location.intY());
			}
			//spawnareas
			output.writeInt(room.spawnAreas.size());
			for (SpawnArea spawnarea : room.spawnAreas) {
				output.writeInt(spawnarea.area.x);
				output.writeInt(spawnarea.area.y);
				output.writeInt(spawnarea.area.width);
				output.writeInt(spawnarea.area.height);
				output.writeInt(spawnarea.maxEnemies);
				output.writeInt(spawnarea.maxSpawns);
				output.writeInt(spawnarea.entityType.id);
			}
			output.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
