package ch.swordsgame.util;

/**
 * An enum for the directions up, down, left and right.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public enum Direction {
	UP,
	DOWN,
	LEFT,
	RIGHT;
}
