package ch.swordsgame.util;

import ch.swordsgame.Swords;

/**
 * The Size class. It holds a width and a height and can convert the world to the screen depending n the zoom of the camera.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class Size {
	
	/**
	 * The width of the size.
	 */
	public int width;
	
	/**
	 * The height of the size.
	 */
	public int height;
	
	/**
	 * The constructor.
	 * 
	 * @param width The width that the size should have.
	 * @param height The height that the size should have.
	 */
	public Size(int width, int height) {
		this.width = width;
		this.height = height;
	}
	
	/**
	 * Converts the given size to a new size depending on the zoom of the camera.
	 * 
	 * @param size The original size.
	 * @return The modified size.
	 */
	public static Size convertWorldToScreen(Size size) {
		return new Size((int) (size.width * Swords.camera.zoom), (int) (size.height * Swords.camera.zoom));
	}
}
