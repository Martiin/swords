package ch.swordsgame;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

/**
 * The spritesheet, which contains one sprite with multiple images.
 * 
 *  @author Martin Bissiger	& Sergej Ilich
 * 
 */
public class SpriteSheet {
	
	private BufferedImage spriteSheet;
	private int tileWidth, tileHeight;
	
	public SpriteSheet() {
		
	}
	
	/**
	 * Set the spritesheet with the size of a tile.
	 * 
	 * @param spriteSheet The spritesheet.
	 * @param tileWidth The width of one tile.
	 * @param tileHeight the height of one tile.
	 */
	public void setSpriteSheet(BufferedImage spriteSheet, int tileWidth, int tileHeight) {
		this.spriteSheet = spriteSheet;
		this.tileWidth = tileWidth;
		this.tileHeight = tileHeight;
	}
	
	/**
	 * Gets a tile from the specified location.
	 * 
	 * @param xTile The X coordinate of the tile.
	 * @param yTile The Y coordinate of the tile.
	 * @return The image from the specified location.
	 */
	public BufferedImage getTile(int xTile, int yTile) {
		return getTileExact(xTile * tileWidth, yTile * tileHeight, tileWidth, tileHeight);
	}
	
	/**
	 * Gets a tile from the specified location.
	 * 
	 * @param xTile The X coordinate of part one of the tile.
	 * @param yTile The Y coordinate of part one of the the tile.
	 * @param xTile The X coordinate of part two of the the tile.
	 * @param yTile The Y coordinate of part two of the the tile.
	 * @return The image from the specified location.
	 */
	public BufferedImage getDoubeTile(int xTile1, int yTile1, int xTile2, int yTile2) {
		BufferedImage tile = new BufferedImage(tileWidth, tileHeight * 2, 6);// 6 = imgType???
		Graphics2D g = tile.createGraphics();
		g.drawImage(getTileExact(xTile1 * tileWidth, yTile1 * tileHeight, tileWidth, tileHeight), 0, 0, null);
		g.drawImage(getTileExact(xTile2 * tileWidth, yTile2 * tileHeight, tileWidth, tileHeight), 0, tileHeight, null);
		g.dispose();
		return tile;
	}
	
	/**
	 * Gets a tile from the specified location with X, Y, width and height.
	 * 
	 * @param xTile The X coordinate of the tile.
	 * @param yTile The Y coordinate of the tile.
	 * @param width The width of the tile.
	 * @param height The height of the tile.
	 * @return The image from the specified location.
	 */
	public BufferedImage getTileExact(int xTile, int yTile, int width, int height) {
		return spriteSheet.getSubimage(xTile, yTile, width, height);
	}
}
