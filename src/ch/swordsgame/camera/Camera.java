package ch.swordsgame.camera;

import java.awt.Rectangle;

import ch.swordsgame.Swords;
import ch.swordsgame.util.Size;
import ch.swordsgame.util.Vector2DFloat;

/**
 * The camera class. It holds the zoom, size and location / position of the camera.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class Camera {
	
	/**
	 * The current position of the camera saved in a Vector2D object.
	 */
	public Vector2DFloat position = new Vector2DFloat();
	
	/**
	 * The size of the camera.
	 */
	public Size size;
	
	/**
	 * The current zoom state.
	 */
	public double zoom = 2.0;
	
	/**
	 * The constructor.
	 * 
	 * @param width The width of the window.
	 * @param height The height of the window.
	 */
	public Camera(int width, int height) {
		size = new Size(width, height);
	}
	
	/**
	 * Returns the area, which needs to be rendered.
	 * 
	 * @return The area, which needs to be rendered saved in a rectangle object.
	 */
	public Rectangle getRenderArea() {
		Vector2DFloat pos = position.copy().subtract(Swords.player.location.copy().subtract(new Vector2DFloat(Swords.size.width, Swords.size.height).divide(4)).add(new Vector2DFloat(Swords.player.size.width, Swords.player.size.height).divide(2)));
		return new Rectangle(pos.intX(), pos.intY(), size.width, size.height);
	}
}
