package ch.swordsgame;

/**
 * The Screenmode (Windowed, borderless window, fullscreen).
 * 
 * @author Martin Bissiger	& Sergej Ilich
 */
public enum ScreenMode {
	WINDOWED,
	BORDERLESS_WINDOW,
	FULLSCREEN;
}