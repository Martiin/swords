package ch.swordsgame.entities;

import java.awt.Rectangle;
import java.util.Random;

import ch.swordsgame.Swords;
import ch.swordsgame.animation.AnimationState;
import ch.swordsgame.entities.enemies.Bat;
import ch.swordsgame.entities.enemies.Slime;
import ch.swordsgame.util.Size;
import ch.swordsgame.util.Vector2DFloat;

/**
 * The Enemy object. It extends from EntityBase and holds information like
 *  @author Martin Bissiger	& Sergej Ilich
 *
 */
public abstract class Enemy extends EntityBase {
	
	private Rectangle spawnArea;
	
	public int viewRange;
	
	private int changeDirectionTimer;
	
	private int changeDirectionTime;
	
	private Vector2DFloat lastLocation;

	protected Enemy(EntityType type, Size size, Vector2DFloat location, Rectangle collider, float maxHealth,
			float maxSpeed, Rectangle spawnArea, int viewRange) {
		super(type, size, location, collider, maxHealth, maxSpeed);
		this.spawnArea = spawnArea;
		this.viewRange = viewRange;
		this.visible = false;
		
		this.damage = 10;
		this.hitRange = 20;
		this.hitDelay = 500;
		this.currentHitDelay = 0;
		
		this.changeDirectionTime = 1000;
	}
	
	/**
	 * Lets the enemy spawn.
	 */
	public void spawn(int x, int y){
		this.location = new Vector2DFloat(x, y);
		this.visible = true;
	}
	
	/**
	 * Executes a tick on the enemy.
	 * 
	 * @param deltatime
	 */
	@Override
	public void tick(double deltatime){
		
		if (this.health <= 0)
			return;
		
		//Get the player
		Player player = Swords.player;
		if (this.location.intX() - this.viewRange - player.location.intX() < 0 &&
			player.location.intX() - this.location.intX() - this.viewRange < 0 &&
			this.location.intY() - this.viewRange - player.location.intY() < 0 &&
			player.location.intY() - this.location.intY() - this.viewRange < 0){
			if (this.location.intX() < player.location.intX())
				right = true;
			else
				right = false;
			if (this.location.intX() > player.location.intX())
				left = true;
			else
				left = false;
			if (this.location.intY() < player.location.intY())
				down = true;
			else
				down = false;
			if (this.location.intY() > player.location.intY())
				up = true;
			else
				up = false;
				
		}
		else{
			changeDirectionTimer -= deltatime;
			
			if (changeDirectionTimer <= 0 || lastLocation.equals(this.location)){
				changeDirectionTimer = new Random().nextInt(1000) + 500;
				right = false;
				left = false;
				down = false;
				up = false;
			
				int action = new Random().nextInt(5);
				
				switch (action){
				case 0:
					right = true;
					break;
				case 1:
					left = true;
					break;
				case 2:
					down = true;
					break;
				case 3:
					up = true;
					break;
				}
			}
			
			lastLocation = this.location.copy();
		}
		
		if (this.location.intX() - this.hitRange - player.location.intX() < 0 &&
			player.location.intX() - this.location.intX() - this.hitRange < 0 &&
			this.location.intY() - this.hitRange - player.location.intY() < 0 &&
			player.location.intY() - this.location.intY() - this.hitRange < 0 &&
			this.currentHitDelay <= 0){
			
			player.TakeDamage(damage);
			this.currentHitDelay = this.hitDelay;
			this.animationState = AnimationState.IDLE_DOWN;
		}
		
		this.currentHitDelay -= deltatime;
		
		super.tick(deltatime);
	}
	
	@Override
	public void TakeDamage(int amount){
		if (health <= 0)
			return;
		super.TakeDamage(amount);
		this.health -= amount;
		if (health <= 0)
			animationState = AnimationState.DEAD;
		//Render other image, that shows, that it takes damage
	}
	
	public static Enemy createFromType(EntityType type, Size size, Vector2DFloat location, Rectangle collider, float maxHealth,
			float maxSpeed, Rectangle spawnArea, int viewRange) {
		switch (type) {
		case SLIME:
			return new Slime(size, location, collider, maxHealth, maxSpeed, spawnArea, viewRange);
		case BAT:
			return new Bat(size, location, collider, maxHealth, maxSpeed, spawnArea, viewRange);
		default:
			return null;
		}
	}
}
