package ch.swordsgame.entities.enemies;

import java.awt.Rectangle;
import java.util.ArrayList;

import ch.swordsgame.entities.Enemy;
import ch.swordsgame.entities.EntityType;;

/**
 * The SpawnArea object. It holds information like the area in which enemies can spawn, how much enemies can spawn, what kind of enemies can spawn, etc.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class SpawnArea {
	
	/**
	 * The area in which enemies can spawn.
	 */
	public Rectangle area;
	
	/**
	 * Sets the max amount of enemies which can spawn in this area.
	 */
	public int maxEnemies;
	
	/**
	 * Sets how many enemies can spawn.
	 */
	public int maxSpawns;
	
	/**
	 * Returns an int with the amount of living enemies in this SpawnArea.
	 */
	public int EnemiesAlive;
	
	/**
	 * Returns an int with the amount of enemies spawned.
	 */
	public int EnemiesSpawned;
	
	/**
	 * The entityTypes determines what kind of entities should be spawned.
	 */
	public EntityType entityType;
	
	/**
	 * The enemies spawned in this area.
	 */
	public ArrayList<Enemy> Enemies;
	
	/**
	 * The constructor.
	 */
	public SpawnArea(){
		this.Enemies = new ArrayList<Enemy>();
	}
}
