package ch.swordsgame.entities.enemies;

import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;

import ch.swordsgame.Assets;
import ch.swordsgame.animation.AnimationState;
import ch.swordsgame.animation.Animator;
import ch.swordsgame.entities.Enemy;
import ch.swordsgame.entities.EntityType;
import ch.swordsgame.util.Size;
import ch.swordsgame.util.Vector2DFloat;

public class Slime extends Enemy {

	public Slime(Size size, Vector2DFloat location, Rectangle collider, float maxHealth, 
			float maxSpeed, Rectangle spawnArea, int viewRange) {
		super(EntityType.SLIME, size, location, collider, maxHealth, maxSpeed, spawnArea, viewRange);
	}

	@Override
	public void init() {
		animators = new HashMap<AnimationState, Animator>();
		
		ArrayList<BufferedImage> framesUp = new ArrayList<BufferedImage>();
		ArrayList<BufferedImage> framesDown = new ArrayList<BufferedImage>();
		ArrayList<BufferedImage> framesLeft = new ArrayList<BufferedImage>();
		ArrayList<BufferedImage> framesRight = new ArrayList<BufferedImage>();
		
		ArrayList<BufferedImage> framesDead = new ArrayList<BufferedImage>();

		framesUp.add(Assets.slime.getTile(2, 1));
		framesUp.add(Assets.slime.getTile(3, 1));
		framesDown.add(Assets.slime.getTile(2, 0));
		framesDown.add(Assets.slime.getTile(3, 0));
		framesLeft.add(Assets.slime.getTile(0, 0));
		framesLeft.add(Assets.slime.getTile(1, 0));
		framesRight.add(Assets.slime.getTile(0, 1));
		framesRight.add(Assets.slime.getTile(1, 1));
		
		framesDead.add(Assets.slime.getTile(0, 2));
		
		animators.put(AnimationState.UP, createAnimation(framesUp));
		animators.put(AnimationState.DOWN, createAnimation(framesDown));
		animators.put(AnimationState.LEFT, createAnimation(framesLeft));
		animators.put(AnimationState.RIGHT, createAnimation(framesRight));
		animators.put(AnimationState.IDLE_DOWN, createAnimation(framesUp));
		animators.put(AnimationState.IDLE_UP, createAnimation(framesDown));
		animators.put(AnimationState.IDLE_RIGHT, createAnimation(framesRight));
		animators.put(AnimationState.IDLE_LEFT, createAnimation(framesLeft));
		animators.put(AnimationState.DEAD, createAnimation(framesDead));
	}
}
