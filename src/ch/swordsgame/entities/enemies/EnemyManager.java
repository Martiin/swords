package ch.swordsgame.entities.enemies;

import java.awt.Rectangle;
import java.util.Collections;
import java.util.Random;
import java.util.Vector;

import ch.swordsgame.entities.Enemy;
import ch.swordsgame.util.Size;
import ch.swordsgame.util.Vector2DFloat;

/**
 * Manages the enemies.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class EnemyManager{
	/**
	 * A List of all spawnareas in this room.
	 */
	Vector<SpawnArea> spawnAreas;
	
	public EnemyManager(Vector<SpawnArea> spawnAreas){
		this.spawnAreas = spawnAreas;
	}
	
	public Enemy spawnEnemy(){
		Collections.shuffle(spawnAreas);
		for (SpawnArea area : spawnAreas)
			if (area.EnemiesAlive < area.maxEnemies && area.EnemiesSpawned < area.maxSpawns){
				area.EnemiesSpawned += 1;
				area.EnemiesAlive += 1;
				Enemy e = spawnEnemyInArea(area);
				area.Enemies.add(e);
				return e;
			}
			
		return null;
	}
	
	private Enemy spawnEnemyInArea(SpawnArea spawnArea){
		Random r = new Random();
		int x = r.nextInt(spawnArea.area.x + (int)spawnArea.area.getWidth() - spawnArea.area.x) + spawnArea.area.x;
		int y = r.nextInt(spawnArea.area.y + (int)spawnArea.area.getHeight() - spawnArea.area.y) + spawnArea.area.y;
		Enemy enemy = Enemy.createFromType(spawnArea.entityType, new Size(32, 32), new Vector2DFloat(x, y), new Rectangle(0, 16, 32, 16), 100, 50, spawnArea.area, 200);
		enemy.init();
		enemy.spawn(x, y);
		return enemy;
	}
}

