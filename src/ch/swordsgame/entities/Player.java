package ch.swordsgame.entities;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;

import ch.swordsgame.Assets;
import ch.swordsgame.Main;
import ch.swordsgame.Swords;
import ch.swordsgame.animation.AnimationState;
import ch.swordsgame.animation.Animator;
import ch.swordsgame.collision.CollisionCheck;
import ch.swordsgame.entities.enemies.SpawnArea;
import ch.swordsgame.manager.DebugManager;
import ch.swordsgame.util.Size;
import ch.swordsgame.util.Vector2DFloat;

/**
 * The class Player.
 * 
 *  @author Martin Bissiger	& Sergej Ilich
 *
 */
public class Player extends EntityBase implements KeyListener {
	/**
	 * How many diamonds the player collected so far.
	 */
	public int diamonds;
	
	/**
	 * Determines, whether the Player should hit an Enemy or not.
	 */
	public boolean hit;
	
	/**
	 * The constructor of the player. The location will be the center of the screen by default.
	 */
	public Player() {
		super(EntityType.PLAYER, new Size(50, 40), new Vector2DFloat(Main.getSize().width / 2 - 40, Main.getSize().height / 2 - 40),
				new Rectangle(10, 30, 20, 10), 100, 200);
	}
	
	@Override
	public void init() {
		this.damage = 25;
		this.hitRange = 75;
		this.hitDelay = 250;
		this.currentHitDelay = 0;
		
		animators = new HashMap<AnimationState, Animator>();
		
		ArrayList<BufferedImage> framesUp = new ArrayList<BufferedImage>();
		ArrayList<BufferedImage> framesDown = new ArrayList<BufferedImage>();
		ArrayList<BufferedImage> framesLeft = new ArrayList<BufferedImage>();
		ArrayList<BufferedImage> framesRight = new ArrayList<BufferedImage>();
		ArrayList<BufferedImage> framesIdleUp = new ArrayList<BufferedImage>();
		ArrayList<BufferedImage> framesIdleDown = new ArrayList<BufferedImage>();
		ArrayList<BufferedImage> framesIdleLeft = new ArrayList<BufferedImage>();
		ArrayList<BufferedImage> framesIdleRight = new ArrayList<BufferedImage>();
		ArrayList<BufferedImage> framesDead = new ArrayList<BufferedImage>();
		ArrayList<BufferedImage> framesFightUp = new ArrayList<BufferedImage>();
		ArrayList<BufferedImage> framesFightDown = new ArrayList<BufferedImage>();
		ArrayList<BufferedImage> framesFightLeft = new ArrayList<BufferedImage>();
		ArrayList<BufferedImage> framesFightRight = new ArrayList<BufferedImage>();
		
		framesUp.add(Assets.player.getTile(1, 1));
		framesUp.add(Assets.player.getTile(1, 2));
		framesDown.add(Assets.player.getTile(0, 1));
		framesDown.add(Assets.player.getTile(0, 2));
		framesLeft.add(Assets.player.getTile(2, 1));
		framesLeft.add(Assets.player.getTile(2, 2));
		framesRight.add(Assets.player.getTile(3, 1));
		framesRight.add(Assets.player.getTile(3, 2));
		framesIdleUp.add(Assets.player.getTile(1, 0));
		framesIdleDown.add(Assets.player.getTile(0, 0));
		framesIdleLeft.add(Assets.player.getTile(2, 0));
		framesIdleRight.add(Assets.player.getTile(3, 0));
		
		framesFightUp.add(Assets.player.getTile(1, 1));
		framesFightUp.add(Assets.player.getTile(1, 2));
		
		framesFightDown.add(Assets.player.getTile(0, 1));
		framesFightDown.add(Assets.player.getTile(0, 2));
		
		framesFightLeft.add(Assets.player.getTile(0, 4));
		framesFightLeft.add(Assets.player.getTile(0, 3));
		
		framesFightRight.add(Assets.player.getTile(1, 4));
		framesFightRight.add(Assets.player.getTile(1, 3));
		
		framesDead.add(Assets.player.getTile(4, 2));
		
		animators.put(AnimationState.UP, createAnimation(framesUp));
		animators.put(AnimationState.DOWN, createAnimation(framesDown));
		animators.put(AnimationState.LEFT, createAnimation(framesLeft));
		animators.put(AnimationState.RIGHT, createAnimation(framesRight));
		animators.put(AnimationState.IDLE_DOWN, createAnimation(framesIdleDown));
		animators.put(AnimationState.IDLE_UP, createAnimation(framesIdleUp));
		animators.put(AnimationState.IDLE_RIGHT, createAnimation(framesIdleRight));
		animators.put(AnimationState.IDLE_LEFT, createAnimation(framesIdleLeft));
		animators.put(AnimationState.FIGHT_UP, createAnimation(framesFightUp));
		animators.put(AnimationState.FIGHT_DOWN, createAnimation(framesFightDown));
		animators.put(AnimationState.FIGHT_LEFT, createAnimation(framesFightLeft));
		animators.put(AnimationState.FIGHT_RIGHT, createAnimation(framesFightRight));
		animators.put(AnimationState.DEAD, createAnimation(framesDead));
	}
	
	private double dieTime = 0;
	
	@Override
	public void tick(double deltaTime) {
		super.tick(deltaTime);
		
		if (CollisionCheck.CollisionWarpPoint(getCollider())) {
			Swords.getLevel().nextRoom();
			return;
		}
		
		if (this.animationState == AnimationState.DEAD) {
			dieTime += deltaTime;
			if (dieTime > 1000) {
				health = maxHealth;
				this.animationState = AnimationState.IDLE_DOWN;
				location = Swords.getLevel().rooms.peek().spawnPoint.copy();
			}
		}

		if (this.health <= 0){
			if (this.animationState != AnimationState.DEAD) {
				this.animationState = AnimationState.DEAD;
				dieTime = 0;
			}
			return;
		}
		
		if (hit && currentHitDelay <= 0){
			currentHitDelay = hitDelay;
			hit = false;
			
			switch (this.animationState)
			{
			case UP:
			case IDLE_UP:
				this.animationState = AnimationState.FIGHT_UP;
				break;
			case DOWN:
			case IDLE_DOWN:
				this.animationState = AnimationState.FIGHT_DOWN;
				break;
			case LEFT:
			case IDLE_LEFT:
				this.animationState = AnimationState.FIGHT_LEFT;
				break;
			case RIGHT:
			case IDLE_RIGHT:
				this.animationState = AnimationState.FIGHT_RIGHT;
				break;
			default:
				break;
			}
			isFighting = true;
			isFightingTime = 30 * deltaTime;
			
			for (SpawnArea sa : Swords.getLevel().rooms.peek().spawnAreas){
				for (Enemy e : sa.Enemies){
					if (this.location.intX() - this.hitRange - e.location.intX() < 0 &&
							e.location.intX() - this.location.intX() - this.hitRange < 0 &&
							this.location.intY() - this.hitRange - e.location.intY() < 0 &&
							e.location.intY() - this.location.intY() - this.hitRange < 0){
						e.TakeDamage(damage);
					}
				}
			}	
		}
		
		this.currentHitDelay -= deltaTime;
	}
	
	@Override
	public void render(Graphics2D g) {
		super.render(g);
		
		Vector2DFloat loc = Vector2DFloat.convertWorldToScreen(location);
		Size s = Size.convertWorldToScreen(size);
		if (DebugManager.PLAYER == 1 || DebugManager.PLAYER == 3) {
			g.setColor(Color.GREEN);
			Size cs = Size.convertWorldToScreen(new Size(collider.width, collider.height));
			Vector2DFloat cloc = Vector2DFloat.convertWorldToScreen(new Vector2DFloat(getCollider().x, getCollider().y));
			g.drawRect(cloc.intX(), cloc.intY(), cs.width, cs.height);
		}
		if (DebugManager.PLAYER == 2 || DebugManager.PLAYER == 3) {
			int size2 = (int) (5 * Swords.camera.zoom);
			int add = 2 * Swords.camera.zoom < 2 ? 0 : 2;
			int now = add;
			g.setColor(new Color(20, 20, 20, 192));
			g.fillRect(loc.intX(), loc.intY(), s.width, s.height);
			g.setStroke(new BasicStroke(1));
			g.setFont(new Font("Consolas", Font.PLAIN, size2));
			g.setColor(Color.CYAN);
			g.drawString("Player", loc.x + 2, loc.y + (now += size2 + add));
			g.setColor(Color.PINK);
			g.drawString((int) (location.x / 32) + "," + (int) (location.y / 32), loc.x + 2, loc.y + (now += size2 + add));
			g.setColor(Color.MAGENTA);
			g.drawString(location.x + ", " + location.x, loc.x + 2, loc.y + (now += size2 + add));
			g.setColor(Color.YELLOW);
			g.drawString(loc.y + ", " + loc.x, loc.x + 2, loc.y + (now += size2 + add));
		}
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_W:
		case KeyEvent.VK_UP:
		case KeyEvent.VK_KP_UP:
			up = true;
			break;
		case KeyEvent.VK_S:
		case KeyEvent.VK_DOWN:
		case KeyEvent.VK_KP_DOWN:
			down = true;
			break;
		case KeyEvent.VK_A:
		case KeyEvent.VK_LEFT:
		case KeyEvent.VK_KP_LEFT:
			left = true;
			break;
		case KeyEvent.VK_D:
		case KeyEvent.VK_RIGHT:
		case KeyEvent.VK_KP_RIGHT:
			right = true;
			break;
		case KeyEvent.VK_SPACE:
			//Check for hit delay, and first implement hit delay XD
			hit = true;
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_W:
		case KeyEvent.VK_UP:
		case KeyEvent.VK_KP_UP:
			up = false;
			break;
		case KeyEvent.VK_S:
		case KeyEvent.VK_DOWN:
		case KeyEvent.VK_KP_DOWN:
			down = false;
			break;
		case KeyEvent.VK_A:
		case KeyEvent.VK_LEFT:
		case KeyEvent.VK_KP_LEFT:
			left = false;
			break;
		case KeyEvent.VK_D:
		case KeyEvent.VK_RIGHT:
		case KeyEvent.VK_KP_RIGHT:
			right = false;
			break;
		}
	}
	
	@Override
	public void TakeDamage(int amount){
		if (health <= 0)
			return;
		super.TakeDamage(amount);
		this.health -= amount;
	}
	
	@Override
	public void keyTyped(KeyEvent e) {}
}