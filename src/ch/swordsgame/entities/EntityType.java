package ch.swordsgame.entities;

/**
 * An enum containing all entity types.
 * 
 *  @author Martin Bissiger	& Sergej Ilich
 *
 */
public enum EntityType {
	PLAYER(0),
	SLIME(1),
	BAT(2);
	
	/**
	 * The id of the EntityType.
	 */
	public final int id;
	
	private EntityType(int id) {
		this.id = id;
	}
	
	/**
	 * Returns a BlockType of the given id.
	 * 
	 * @param id The id which should match the BlockType.
	 * @return A BlockType matching the given id.
	 */
	public static EntityType byId(int id) {
		for (EntityType bt : EntityType.values()) {
			if (bt.id == id) {
				return bt;
			}
		}
		return null;
	}
}
