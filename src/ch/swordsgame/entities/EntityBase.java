package ch.swordsgame.entities;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;

import ch.swordsgame.GameLoop;
import ch.swordsgame.Renderable;
import ch.swordsgame.Swords;
import ch.swordsgame.animation.AnimationState;
import ch.swordsgame.animation.Animator;
import ch.swordsgame.collision.CollisionCheck;
import ch.swordsgame.level.blocks.Block;
import ch.swordsgame.manager.DebugManager;
import ch.swordsgame.util.Direction;
import ch.swordsgame.util.Size;
import ch.swordsgame.util.Vector2DFloat;

/**
 * The base class of all entities.
 * 
 *  @author Martin Bissiger	& Sergej Ilich
 *
 */
public abstract class EntityBase extends Renderable {
	private float fixDeltaTime = (float) (1f / GameLoop.FPS);
	
	/**
	 * The type of the entity
	 */
	public EntityType type;
	
	/**
	 * Determines wheter the entity is visible or not.
	 */
	public boolean visible;
	
	// movement & speed
	/**
	 * Indicates if the entity is currently moving in the specific direction.
	 */
	protected boolean up, down, right, left;
	
	/**
	 * Determining how fast the entity is moving in which direction.
	 */
	private float speedUp = 0, speedDown = 0, speedLeft = 0, speedRight = 0;
	
	/**
	 * The range where the entity can hit another entity
	 */
	protected int hitRange;
	
	/**
	 * The damage affected with one hit.
	 */
	protected int damage;
	
	/**
	 * The time how long it takes to wait until the next hit can be done.
	 */
	protected int hitDelay;
	
	/**
	 * The time left, how long it takes to wait until the next hit can be done.
	 */
	protected int currentHitDelay;
	
	/**
	 * How fast the entity should speed up to the max speed and slow down to 0. Defaults to 10.
	 */
	public float glide = 10f;
	
	/**
	 * Tells whether the entity is currently moving in any direction or not.
	 */
	public boolean isMoving;
	
	// animation
	/**
	 * With what scale the entity should be rendered on the screen. Defaults to 1.
	 */
	public int scale = 1;
	
	/**
	 * How often the frame of the current animation should change in milliseconds. Defaults to 180.
	 */
	public long animationSpeed = 180;
	
	/**
	 * The current animation state of the entity. Defaults to IDE_DOWN.
	 */
	protected AnimationState animationState = AnimationState.IDLE_DOWN;
	
	/**
	 * A HashMap containing the specific animation for each AnimationState.
	 */
	protected HashMap<AnimationState, Animator> animators;
	
	// collider

	/**
	 * The box collider of the entity.
	 */
	protected Rectangle collider;
	
	// variables
	/**
	 * The maximum speed the entity can achieve. Used for all directions.
	 */
	public float maxSpeed = 200;
	
	/**
	 * The maximum health the entity can have.
	 */
	public float maxHealth;
	
	/**
	 * The current health of the entity.
	 */
	public float health;
	
	/**
	 * Every time the entity takes damage, it blinks, so the player can see, who is taking damage. This int determines how many blinks are left until it stops blinking
	 */
	private int damageBlinksLeft;
	
	/**
	 * Determines how often the entity blinks, when it gets hit.
	 */
	private int damageBlinks;

	/**
	 * Determines if the entity is currently fighting
	 */
	public boolean isFighting = true;
	
	/**
	 * Determines if the entity is currently fighting
	 */
	public double isFightingTime = 1;
	
	/**
	 * 
	 * @param type The type of the entity.
	 * @param size The size of the entity.
	 * @param location The start location of the entity.
	 * @param collider The box collider of the entity.
	 * @param maxHealth The maximum health of the entity.
	 * @param maxSpeed The maximum speed of the entity.
	 */
	public EntityBase(EntityType type, Size size, Vector2DFloat location, Rectangle collider, float maxHealth, float maxSpeed) {
		super(50, location, size);
		this.type = type;
		this.collider = collider;
		this.maxHealth = this.health = maxHealth;
		this.maxSpeed = maxSpeed;
		this.visible = true;
		this.damageBlinks = 20;
	}
	
	/**
	 * The initialization of the entity. Set up animation here.
	 */
	public abstract void init();
	
	/**
	 * Executes the actions that need to be executed on each game tick.
	 * @param deltaTime The time which has passed since the last tick.
	 */
	public void tick(double deltaTime) {
		
		if (this.health <= 0)
			return;
		
		//movement
		isMoving = up || down || left || right;
		float sUp = (speedUp = getNewSpeed(up, speedUp, Direction.UP)) * fixDeltaTime;
		float sDown = (speedDown = getNewSpeed(down, speedDown, Direction.DOWN)) * fixDeltaTime;
		float sLeft = (speedLeft = getNewSpeed(left, speedLeft, Direction.LEFT)) * fixDeltaTime;
		float sRight = (speedRight = getNewSpeed(right, speedRight, Direction.RIGHT)) * fixDeltaTime;
		
		Vector2DFloat velocity = new Vector2DFloat(sRight - sLeft, sDown - sUp);
		location.add(velocity);
		
		if (!isFighting && isFightingTime < 2)
		{
			//animation
			if (up) {
				animationState = AnimationState.UP;
			} else if(down) {
				animationState = AnimationState.DOWN;
			} else if (left) {
				animationState = AnimationState.LEFT;
			} else if (right) {
				animationState = AnimationState.RIGHT;
			} else {
				switch (animationState){
				case RIGHT:
				case FIGHT_RIGHT:
					animationState = AnimationState.IDLE_RIGHT;
					break;
				case LEFT:
				case FIGHT_LEFT:
					animationState = AnimationState.IDLE_LEFT;
					break;
				case UP:
				case FIGHT_UP:
					animationState = AnimationState.IDLE_UP;
					break;
				case DOWN:
				case FIGHT_DOWN:
					animationState = AnimationState.IDLE_DOWN;
					break;
				default:
						break;
				}
			}
		}
		else
		{
			isFightingTime--;
			isFighting = false;
		}
	}
	
	/**
	 * Renders the entity on the screen.
	 * @param g The Graphics2D object the entity should be rendered to.
	 */
	public void render(Graphics2D g) {
		if (this.damageBlinksLeft > 0){
			damageBlinksLeft -= 1;
			if (damageBlinksLeft == 0 || damageBlinksLeft % 2 == 0)
				visible = !visible;
		}
		
		if (!visible)
			return;
		
		Vector2DFloat loc = Vector2DFloat.convertWorldToScreen(location);
		Size s = Size.convertWorldToScreen(size);
		
		if (animators.containsKey(animationState)) {
			g.drawImage(animators.get(animationState).sprite, loc.intX(), loc.intY(), s.width, s.height, null);
			boolean update = false;
			switch (animationState) {
			case UP:
				update = up;
				break;
			case DOWN:
				update = down;
				break;
			case LEFT:
				update = left;
				break;
			case RIGHT:
				update = right;
				break;
			default:
				update = true;
				break;
			}
			if (update) {
				animators.get(animationState).update(System.currentTimeMillis());
			}
		}

		if (!(this instanceof Player)) {
			if (DebugManager.ENEMY == 1 || DebugManager.ENEMY == 3) {
				g.setColor(Color.GREEN);
				Size cs = Size.convertWorldToScreen(new Size(collider.width, collider.height));
				Vector2DFloat cloc = Vector2DFloat.convertWorldToScreen(new Vector2DFloat(getCollider().x, getCollider().y));
				g.drawRect(cloc.intX(), cloc.intY(), cs.width, cs.height);
			}
			if (DebugManager.ENEMY == 2 || DebugManager.ENEMY == 3) {
				int size2 = (int) (5 * Swords.camera.zoom);
				int add = 2 * Swords.camera.zoom < 2 ? 0 : 2;
				int now = add;
				g.setColor(new Color(20, 20, 20, 192));
				g.fillRect(loc.intX(), loc.intY(), s.width, s.height);
				g.setStroke(new BasicStroke(1));
				g.setFont(new Font("Consolas", Font.PLAIN, size2));
				g.setColor(Color.CYAN);
				g.drawString(this.type.name(), loc.x + 2, loc.y + (now += size2 + add));
				g.setColor(Color.PINK);
				g.drawString((int) (location.x / 32) + "," + (int) (location.y / 32), loc.x + 2, loc.y + (now += size2 + add));
				g.setColor(Color.MAGENTA);
				g.drawString(location.x + ", " + location.x, loc.x + 2, loc.y + (now += size2 + add));
				g.setColor(Color.YELLOW);
				g.drawString(loc.y + ", " + loc.x, loc.x + 2, loc.y + (now += size2 + add));
			}
		}
	}
	
	private float getNewSpeed(boolean active, float speed, Direction direction) {
		if (active) {
			if (speed < maxSpeed) {
				speed += glide;
			}
			else if (speed > maxSpeed) {
				speed -= glide * 2;
			}
		} else {
			if (speed > maxSpeed) {
				speed -= glide * 2;
			} else {
				speed -= glide;
			}
		}
		if (speed > maxSpeed) {
			speed = maxSpeed;
		} else if (speed < 0) {
			speed = 0;
		}
		Rectangle newColldier = getCollider();
		float deltaSpeed = (float) Math.ceil(speed * fixDeltaTime);
		switch (direction) {
			case UP:
				newColldier.y -= deltaSpeed;
				break;
			case DOWN:
				newColldier.y += deltaSpeed;
				break;
			case LEFT:
				newColldier.x -= deltaSpeed;
				break;
			case RIGHT:
				newColldier.x += deltaSpeed;
				break;
		}
		Vector<Block> blocks = collidedWith(newColldier);
		for (Block b : blocks) {
			if (b.isSolid) {
				speed = 0;
				break;
			}
		}
		return speed;
	}
	
	/**
	 * The box collider at the current in-world position
	 * @return A Rectangle object representing the box collider.
	 */
	public Rectangle getCollider() {
		Rectangle c = (Rectangle) collider.clone();
		c.x += location.x;
		c.y += location.y;
		return c;
	}
	
	/**
	 * Creates a new Animator object out of some BufferedImages.
	 * @param frames The frames for the new animation.
	 * @return An Animatior containing the given frames.
	 */
	protected Animator createAnimation(ArrayList<BufferedImage> frames) {
		Animator ani = new Animator(frames);
		ani.setSpeed(animationSpeed);
		ani.play();
		return ani;
	}
	
	private Vector<Block> collidedWith(Rectangle collider) {
		return CollisionCheck.CollisionEntityBlock(collider);
	}
	
	public void TakeDamage(int amount){
		this.damageBlinksLeft += this.damageBlinks;
	}
}