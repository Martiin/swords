package ch.swordsgame;

import java.awt.Cursor;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;

import ch.swordsgame.GameWindow;
import ch.swordsgame.ScreenMode;
import ch.swordsgame.listener.KeyboardListener;

public class Main {
	public static GameWindow window;
	private static Dimension size;
	
	/**
	 * The entrypoint of the application.
	 * 
	 * @param args The arguments which are given to the application.
	 */
	public static void main(String[] args) {
		int screen = 0;
		if (args.length > 0) {
			try {
				screen = Integer.parseInt(args[0]);
			} catch (Exception e) {}
		}
		window = new GameWindow("Swords", screen);
		window.setFullscreen(ScreenMode.BORDERLESS_WINDOW);
		size = window.getSize();
		Toolkit toolkit = Toolkit.getDefaultToolkit();
		Cursor cursor = toolkit.createCustomCursor(toolkit.getImage(""), new Point(0, 0), "Cursor");
		window.setCursor(cursor);
		
		/*
		BufferedImage image = new BufferedImage(size.width, size.height, BufferedImage.TYPE_INT_RGB);
		Graphics2D graphics2D = (Graphics2D) image.getGraphics();
		graphics2D.drawString("Loading ...", 20, 20);*/

		//initialize
		new Swords(size);
		
		window.addMouseListener(Swords.mouseManager);
		window.addMouseMotionListener(Swords.mouseManager);
		window.addMouseWheelListener(Swords.mouseManager);
		
		window.addKeyListener(Swords.player);
		window.addKeyListener(new KeyboardListener());
		window.add(Swords.gameLoop);
		window.setVisible(true);
	}
	
	/**
	 * ???????????????????????????????????????????????????????????????????????????????
	 * @return ???????????????????????????????????????????????????????????????????????
	 */
	public static Dimension getSize() {
		return size;
	}
}
