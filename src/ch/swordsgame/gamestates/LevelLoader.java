package ch.swordsgame.gamestates;

import java.awt.Graphics2D;

import ch.swordsgame.Swords;
import ch.swordsgame.gamestate.GameState;
import ch.swordsgame.gamestate.GameStateManager;
import ch.swordsgame.level.Difficulty;
import ch.swordsgame.level.Level;
import ch.swordsgame.util.Vector2DFloat;

/**
 * The LevelLoader loads and initializes the levels.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 * 
 */
public class LevelLoader extends GameState {
	
	/**
	 * The current level.
	 */
	public Level level;
	
	public LevelLoader(GameStateManager gsm) {
		super(gsm);
	}

	/**
	 * Initializes the current level and loads a room into it.
	 */
	@Override
	public void init() {
		level = new Level(5, Difficulty.EASY);
		level.init();
	}

	/**
	 * Executes a tick on the current level.
	 * 
	 * @param deltaTime The time which has passed since the last tick.
	 */
	@Override
	public void tick(double deltaTime) {
		if (level != null) {
			level.tick(deltaTime);
			Swords.camera.position = Swords.player.location.copy().multiply(2).subtract(new Vector2DFloat(Swords.size.width / 2 - Swords.player.size.width, Swords.size.height / 2 - Swords.player.size.height));
		}
	}

	/**
	 * Renders a frame on the current level.
	 * @param g The graphics class, which should be used to render.
	 */
	@Override
	public void render(Graphics2D g) {
		if (level != null) {
			level.render(g);
		}
	}
}
