package ch.swordsgame.gamestates;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.RenderingHints;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Vector;

import javax.swing.JOptionPane;

import ch.swordsgame.Button;
import ch.swordsgame.GameWindow;
import ch.swordsgame.Main;
import ch.swordsgame.Swords;
import ch.swordsgame.gamestate.GameState;
import ch.swordsgame.gamestate.GameStateManager;

/**
 * The LevelLoader loads and initializes the levels.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 * 
 */
public class Menu extends GameState {
	
	private Vector<Button> buttons;
	
	public Menu(GameStateManager gsm) {
		super(gsm);
	}

	/**
	 * Initializes the current level and loads a room into it.
	 */
	@Override
	public void init() {
		buttons = new Vector<Button>();
		int center = (int) Swords.size.width / 2;
		Main.window.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if (Swords.gameStateManager.states.peek() instanceof Menu) {
					if (e.getButton() == MouseEvent.BUTTON1) {
						executeClickEvent(e.getPoint());
					}	
				}
			}
		}); 
		buttons.add(new Button("New Run", center - 175, 340, 350, 50) {
			@Override
			public void click() {
				LevelLoader state = new LevelLoader(Swords.gameStateManager);
				state.init();
				Swords.gameStateManager.states.push(state);
			}
		});
		/*buttons.add(new Button("Continue Run", center - 175, 400, 350, 50) {
			@Override
			public void click() {
				JOptionPane.showMessageDialog(null, "in work (not really)", "Sorry", JOptionPane.WARNING_MESSAGE);
			}
		});*/
		buttons.add(new Button("Exit", center - 175, 460, 350, 50) {
			@Override
			public void click() {
				System.exit(1);
			}
		});
	}
	
	private void executeClickEvent(Point p) {
		for (Button b : buttons) {
			if (b.contains(p)) {
				b.click();
			}
		}
	}

	/**
	 * Executes a tick on the current level.
	 * 
	 * @param deltaTime The time which has passed since the last tick.
	 */
	@Override
	public void tick(double deltaTime) {
		int center = (int) Swords.size.width / 2;
		for (Button b : buttons) {
			b.x = center - b.width / 2;
		}
	}

	/**
	 * Renders a frame on the current level.
	 * @param g The graphics class, which should be used to render.
	 */
	@Override
	public void render(Graphics2D g) {
		int center = (int) Swords.size.width / 2;
		g.setStroke(new BasicStroke(20));
		g.setFont(new Font("Consolas", Font.PLAIN, 250));
		g.setColor(Color.blue);
		g.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		g.drawString("Swords", center - 270, 280);
		for (Button b : buttons) {
			b.render(g);
		}
	}
}
