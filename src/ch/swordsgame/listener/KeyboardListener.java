package ch.swordsgame.listener;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;

import ch.swordsgame.manager.DebugManager;

/**
 * The KeyboardListener implements KeyListener and listens to key strokes.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class KeyboardListener implements KeyListener {

	/**
	 * The function which is called when a key is pressed on the keyboard.
	 * 
	 * @param e The KeyEvent.
	 */
	@Override
	public void keyPressed(KeyEvent e) {
		switch (e.getKeyCode()) {
		case KeyEvent.VK_F3:
			DebugManager.DEBUGGER = !DebugManager.DEBUGGER;
			break;
		case KeyEvent.VK_F5:
			DebugManager.TILES = (byte) (DebugManager.TILES < 3 ? DebugManager.TILES + 1 : 0);
			break;
		case KeyEvent.VK_F6:
			DebugManager.BLOCKS = (byte) (DebugManager.BLOCKS < 3 ? DebugManager.BLOCKS + 1 : 0);
			break;
		case KeyEvent.VK_F7:
			DebugManager.PLAYER = (byte) (DebugManager.PLAYER < 3 ? DebugManager.PLAYER + 1 : 0);
			break;
		case KeyEvent.VK_F8:
			DebugManager.SPAWN = !DebugManager.SPAWN;
			break;
		case KeyEvent.VK_F9:
			DebugManager.WARP = !DebugManager.WARP;
			break;
		case KeyEvent.VK_F10:
			DebugManager.ENEMY = (byte) (DebugManager.ENEMY < 3 ? DebugManager.ENEMY + 1 : 0);
			break;
		case KeyEvent.VK_ESCAPE:
			System.exit(0);
			break;
		default:
			break;
		}
	}

	/**
	 * The function which is called when a key is released from the keyboard.
	 * 
	 * @param e The KeyEvent.
	 */
	@Override
	public void keyReleased(KeyEvent e) {
		
	}

	/**
	 * The function which is called when a key is typed on the keyboard.
	 * 
	 * @param e The KeyEvent.
	 */
	@Override
	public void keyTyped(KeyEvent e) {
		
	}
}
