package ch.swordsgame;

import java.awt.Graphics2D;
import java.awt.Rectangle;

import ch.swordsgame.util.Size;
import ch.swordsgame.util.Vector2DFloat;

/**
 * The base class for all renderable objects.
 * 
 *  @author Martin Bissiger	& Sergej Ilich
 *
 */
public abstract class Renderable implements Comparable<Renderable> {
	
	/**
	 * Whether the block needs to be rendered or not.
	 */
	public boolean isActive;
	
	/**
	 * The in-world-location of the object.
	 */
	public Vector2DFloat location;
	
	/**
	 * The zIndex of the object.
	 */
	public int zIndex;
	
	/**
	 * The size of the object.
	 */
	public Size size;
	
	/**
	 * The constructor.
	 * @param zIndex The zIndex of the object.
	 * @param location The in-world-location of the object.
	 * @param size The size of the object.
	 */
	public Renderable(int zIndex, Vector2DFloat location, Size size) {
		this.zIndex = zIndex;
		this.location = location;
		this.size = size;
	}
	
	/**
	 * Get a Rectangle representing the whole object. Used for renering.
	 * @return A Rectangle object representing the object.
	 */
	public Rectangle getBounds() {
		return new Rectangle(location.intX(), location.intY(), size.width, size.height);
	}
	
	/**
	 * Renders the object on the screen.
	 * @param g The Graphics2D object the object should be rendered to.
	 */
	public abstract void render(Graphics2D g);

	/**
	 * Current lowest Y position of the object.
	 * return The Y position.
	 */
	public int getLowestY() {
		return (int) (location.y + size.height);
	}
	
	@Override
	public int compareTo(Renderable other) {
		if (this.zIndex != other.zIndex) {
			return Integer.compare(this.zIndex, other.zIndex);
		} else {
			return Integer.compare(this.getLowestY(), other.getLowestY());
		}
	}
}
