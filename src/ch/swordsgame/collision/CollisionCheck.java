package ch.swordsgame.collision;

import java.awt.Rectangle;
import java.util.Vector;

import ch.swordsgame.Swords;
import ch.swordsgame.level.blocks.Block;

/**
 * The class which checks the collision of several objects.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class CollisionCheck {
	
	/**
	 * Checks whether an entity collides with the warppoint of the current room.
	 * 
	 * @param collider The collider of the player
	 * @return true if there's a collision.
	 */
	public static boolean CollisionWarpPoint(Rectangle collider) {
		return Collides(collider, Swords.getLevel().rooms.peek().warpPoint.getBounds());
	}	
	
	/**
	 * Checks whether an entity collides with an object or not.
	 * 
	 * @param collider The collider of the player
	 * @return The block the entity collided / collides with.
	 */
	public static Vector<Block> CollisionEntityBlock(Rectangle collider) {
		Vector<Block> colblocks = new Vector<Block>();
		Vector<Block> blocks = Swords.getLevel().rooms.peek().blocks;
		for (Block b : blocks){
			boolean collides = Collides(collider, b.getColliders());
			if (collides)
				colblocks.add(b);
		}
		return colblocks;
	}
	
	/**
	 *  Checks whether to rectangles collides.
	 * @param r1 The first rectangle.
	 * @param r2 The second rectangle.
	 * @return A boolean which determines whether the two rectangles collide or not.
	 */
	private static boolean Collides(Rectangle r1, Rectangle r2) {
		return r1.intersects(r2);
	}
	
	/**
	 *  Checks whether to rectangles collides.
	 * @param r1 The first rectangle.
	 * @param r2 The other rectangles.
	 * @return A boolean which determines whether the two rectangles collide or not.
	 */
	private static boolean Collides(Rectangle r1, Vector<Rectangle> r2) {
		for (Rectangle r : r2) {
			if (r1.intersects(r)) {
				return true;
			}
		}
		return false;
	}
}
