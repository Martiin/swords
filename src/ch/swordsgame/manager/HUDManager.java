package ch.swordsgame.manager;

import java.awt.BasicStroke;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;

import ch.swordsgame.Assets;
import ch.swordsgame.Swords;
import ch.swordsgame.util.Vector2DFloat;

/**
 * The HUDManager manages a debug-simmilar window on the left side of the screen
 * 
 *@author Martin Bissiger	& Sergej Ilich
 *
 */
public class HUDManager {
	
	/**
	 * Renders the HUD.
	 * 
	 * @param g The graphics class which should be used to render GUI.
	 */
	public void render(Graphics2D g) {
		int x = 0;
		int y = 0;
		int width = Swords.size.width;
		int height = Swords.size.height;
		
		//health bar
		double percent = (double) Swords.player.health / (double) Swords.player.maxHealth;
		g.drawImage(Assets.player.getTileExact(11, 0, 18, 19), width - 276, height - 60, 32, 32, null);
		g.setColor(new Color(255 - (int) (percent * 255), (int) (percent * 255), 0));
		g.fillRect(width - 230, height - 52, (int) (percent * 200), 20);
		g.setColor(Color.darkGray);
		g.setStroke(new BasicStroke(2));
		g.drawRect(width - 230, height - 52, 200, 20);
		
		//diamonds
		g.drawImage(Assets.diamond, width - 120, height - 100, 32, 32, null);
		g.setColor(Color.cyan);
		g.setFont(new Font("Razer Header Regular", Font.PLAIN, 32));
		g.drawString("x" + Swords.player.diamonds, width - 72, height - 68);
		
		//Debug display
		if (DebugManager.DEBUGGER) {
			int fontsize = 24;
			int space = 36 / 4;
			int n = fontsize + space;
			int w = fontsize + 3 * space;
			int currentPos = 20 + fontsize;
			g.setColor(new Color(20, 20, 20, 128));
			g.fillRect(x, y, fontsize / 2 * 35, fontsize + 14 * n + 10 * w + 2 * space);
			g.setColor(Color.white);
			g.setFont(new Font("Razer Header Regular", Font.PLAIN, fontsize));
			g.drawString("FPS: " + Swords.gameLoop.getFPS(), 20, currentPos);
			g.drawString("TPS: " + Swords.gameLoop.getTPS(), 20, currentPos += n);
			g.drawString("Player", 20, currentPos += w);
			g.drawString("X: " + Vector2DFloat.convertWorldToScreen(Swords.player.location).intX(), 20, currentPos += n);
			g.drawString("Y: " + Vector2DFloat.convertWorldToScreen(Swords.player.location).intY(), 20, currentPos += n);
			g.drawString("X World: " + Swords.player.location.x, 20, currentPos += n);
			g.drawString("Y World: " + Swords.player.location.y, 20, currentPos += n);
			g.drawString("Camera", 20, currentPos += w);
			g.drawString("Width: " + Swords.camera.size.width, 20, currentPos += n);
			g.drawString("Height: " + Swords.camera.size.height, 20, currentPos += n);
			g.drawString("X: " + Swords.camera.position.x, 20, currentPos += n);
			g.drawString("Y: " + Swords.camera.position.y, 20, currentPos += n);
			g.drawString("Mouse", 20, currentPos += w);
			g.drawString("X: " + Swords.mouseManager.mouse.x, 20, currentPos += n);
			g.drawString("Y: " + Swords.mouseManager.mouse.y, 20, currentPos += n);
			g.drawString("X World: " + Vector2DFloat.convertScreenToWorld(Swords.mouseManager.mouse).x, 20, currentPos += n);
			g.drawString("Y World: " + Vector2DFloat.convertScreenToWorld(Swords.mouseManager.mouse).y, 20, currentPos += n);
			g.drawString("Max. Speed: " + Swords.player.maxSpeed, 20, currentPos += w);
			g.drawString("Current Speed", 20, currentPos += w);
		}
	}

}
