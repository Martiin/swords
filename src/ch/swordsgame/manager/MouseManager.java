package ch.swordsgame.manager;
import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseWheelEvent;
import java.awt.event.MouseWheelListener;

import ch.swordsgame.util.Vector2DFloat;

/**
 * The MouseManager implements MouseListener, MouseMotionListener and MouseWheelListener, so it can manage every event of the mouse.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class MouseManager implements MouseListener, MouseMotionListener, MouseWheelListener {
	
	/**
	 * The location of the mouse.
	 */
	public Vector2DFloat mouse = new Vector2DFloat();
	
	/**
	 * Renders the mouse as a square on the screen.
	 * 
	 * @param g The graphics class which should be used to render the tile or block.
	 */
	public void render(Graphics2D g) {
		g.setColor(Color.yellow);
		g.fillRect(mouse.intX() - 2, mouse.intY() - 2, 4, 4);
	}
	
	/**
	 * The function which is called when a mousebutton is pressed.
	 * 
	 * @param e The MouseEvent.
	 */
	@Override
	public void mousePressed(MouseEvent e) {
		
	}

	/**
	 * The function which is called when a mousebutton is released.
	 * 
	 * @param e The MouseEvent.
	 */
	@Override
	public void mouseReleased(MouseEvent e) {
		
	}

	/**
	 * The function which is called when the mousewheel is moved.
	 * 
	 * @param e The MouseWheelEvent.
	 */
	@Override
	public void mouseWheelMoved(MouseWheelEvent e) {
		
	}

	/**
	 * The function which is called when the mouse is dragged on the screen.
	 * 
	 * @param e The MouseEvent.
	 */
	@Override
	public void mouseDragged(MouseEvent e) {
		mouse.x = e.getX();
		mouse.y = e.getY();
	}

	/**
	 * The function which is called when the mouse is moved over the screen.
	 * 
	 * @param e The MouseEvent.
	 */
	@Override
	public void mouseMoved(MouseEvent e) {
		mouse.x = e.getX();
		mouse.y = e.getY();
	}

	/**
	 * The function which is called when a mousebutton is clicked.
	 * 
	 * @param e The MouseEvent.
	 */
	@Override
	public void mouseClicked(MouseEvent e) {
		
	}

	/**
	 * The function which is called when the mouse entered the application.
	 * 
	 * @param e The MouseEvent.
	 */
	@Override
	public void mouseEntered(MouseEvent e) {
		
	}

	/**
	 * The function which is called when the mouse leaves the application.
	 * 
	 * @param e The MouseEvent.
	 */
	@Override
	public void mouseExited(MouseEvent e) {
		
	}
}