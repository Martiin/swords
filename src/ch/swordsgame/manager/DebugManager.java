package ch.swordsgame.manager;

/**
 * The DebugManager manages which things should be debugged or not.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class DebugManager {
	
	/**
	 * Whether to display the debugger HUD.
	 */
	public static boolean DEBUGGER = false;
	
	/**
	 * Whether to display debug info for tiles.
	 * 0 = off
	 * 1 = collider
	 * 2 = info
	 * 3 = collider & info
	 */
	public static byte TILES = 0;
	
	/**
	 * Whether to display debug info for block.
	 * 0 = off
	 * 1 = collider
	 * 2 = info
	 * 3 = collider & info
	 */
	public static byte BLOCKS = 0;
	
	/**
	 * Whether to display debug info for the spawn point
	 */
	public static boolean SPAWN = false;
	
	/**
	 * Whether to display debug info for the teleporter.
	 */
	public static boolean WARP = false;
	
	/**
	 * Whether to display debug info for the player.
	 */
	public static byte PLAYER = 0;
	
	/**
	 * Whether to display debug info for enemies.
	 */
	public static byte ENEMY = 0;
}
