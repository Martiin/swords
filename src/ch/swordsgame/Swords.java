package ch.swordsgame;

import java.awt.Dimension;
import java.awt.Rectangle;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import ch.swordsgame.camera.Camera;
import ch.swordsgame.entities.Player;
import ch.swordsgame.gamestate.GameStateManager;
import ch.swordsgame.gamestates.LevelLoader;
import ch.swordsgame.gamestates.Menu;
import ch.swordsgame.level.Level;
import ch.swordsgame.level.room.Rooms;
import ch.swordsgame.manager.MouseManager;
import ch.swordsgame.util.Size;

/**
 * The class which holds all elements.
 * 
 *  @author Martin Bissiger	& Sergej Ilich
 *
 */
public class Swords {
	private static Swords instance;

	/**
	 * The assets class. It contains all assets of the game.
	 */
	public static Assets assets;
	
	/**
	 * The main game loop.
	 */
	public static GameLoop gameLoop;
	
	/**
	 * The player object.
	 */
	public static Player player;
	
	/**
	 * The camera.
	 */
	public static Camera camera;
	
	/**
	 * The mouse manager. It manages the mouse.
	 */
	public static MouseManager mouseManager;
	
	/**
	 * The game state manager. It handles tick, render and init.
	 */
	public static GameStateManager gameStateManager;
	
	/**
	 * The resolution of the window.
	 */
	public static Dimension size;
	
	/**
	 * The constructor. It initializes all elements and assigns the variables.
	 * 
	 * @param size The resolution of the window.
	 */
	public Swords(Dimension size) {
		if (instance == null) {
			instance = this;
		} else {
			new Exception("An instance of TheGame already exists!").printStackTrace();
		}
		Swords.size = size;
		assets = new Assets();
		assets.init();
		new Rooms().init();
		mouseManager = new MouseManager();
		camera = new Camera(Main.getSize().width, Main.getSize().height);
		player = new Player();
		player.init();
		gameStateManager = new GameStateManager();
		gameStateManager.states.push(new Menu(gameStateManager));
		gameStateManager.init();
		gameLoop = new GameLoop(size);
	}
	
	public static Level getLevel() {
		if (gameStateManager.states.peek() instanceof LevelLoader) {
			return ((LevelLoader) gameStateManager.states.peek()).level;
		}
		return null;
	}
}
