package ch.swordsgame;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics2D;
import java.awt.Rectangle;

public abstract class Button extends Rectangle {
	public String text;
	
	public Button(String text, int x, int y, int width, int height) {
		super(x, y, width, height);
		this.text = text;
	}
	
	public void render(Graphics2D g) {
		g.setFont(new Font("Segoe Print", Font.PLAIN, height));
		g.setColor(Color.gray);
		g.fillRect(x, y, width, height);
		g.setColor(Color.white);
		g.drawString(text, x, y + height - 5);
	}
	
	public abstract void click();
}
