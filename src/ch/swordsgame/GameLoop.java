package ch.swordsgame;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import javax.swing.JPanel;

/**
 * The main game loop, which derives from JPanel and implements Runnable.
 * 
 *  @author Martin Bissiger	& Sergej Ilich
 *
 */
public class GameLoop extends JPanel implements Runnable {
	/**
	 * The MAX FPS variable.
	 */
	public static double FPS = 120d;
	
	/**
	 * The Graphics2D class is used to draw the elements on the screen.
	 */
	public Graphics2D graphics2D;
	
	protected BufferedImage image;
	
	protected Thread thread;
	protected boolean running;
	
	protected int fps;
	protected int tps;
	
	protected Dimension dimension;
	
	/**
	 * The constructor for the game loop.
	 * @param dimension The size of the window.
	 */
	public GameLoop(Dimension dimension) {
		this.dimension = dimension;
		
		setPreferredSize(dimension);
		setFocusable(false);
		requestFocus();
	}
	
	/**
	 * ???????????????????????????????????????????????????????????????????????????????????????????????????
	 */
	@Override
	public void addNotify() {
		super.addNotify();
		if (thread == null) {
			thread = new Thread(this);
			thread.start();
		}
	}

	/**
	 * This is the method which actually contains the loop. Calling run starts the loop.
	 */
	@Override
	public void run() {
		init();
		
		long lastTime = System.nanoTime();
		double nsPerTick = 1000000000d / FPS;
		int frames = 0;
		int ticks = 0;
		long lastTimer = System.currentTimeMillis();
		double deltaTime = 0;
		
		while (running) {
			long now = System.nanoTime();
			deltaTime += (now - lastTime) / nsPerTick;
			lastTime = now;
			boolean shouldRender = false;
			
			while(deltaTime >= 1) {
				ticks++;
				tick(deltaTime);
				deltaTime -= 1;
				shouldRender = true;
			}
			
			if (shouldRender) {
				frames++;
				render();
			}
			
			try {
				Thread.sleep(1);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
			
			if (System.currentTimeMillis() - lastTimer >= 1000) {
				lastTimer += 1000;
				tps = frames;
				fps = ticks;
				frames = 0;
				ticks = 0;
			}
		}
	}

	/**
	 * The initialization of the game loop.
	 */
	public void init() {
		image = new BufferedImage(dimension.width, dimension.height, BufferedImage.TYPE_INT_RGB);
		graphics2D = (Graphics2D) image.getGraphics();
		running = true;
	}

	/**
	 * At every tick, this method is called, which calls the tick in the gameStateManager.
	 * 
	 * @param deltaTime The time which passed since the last tick.
	 */
	public void tick(double deltaTime) {
		Swords.gameStateManager.tick(deltaTime);
	}
	
	/**
	 * Resets the Screen and calls render in the gameStateManager.
	 */
	public void render() {
		graphics2D.clearRect(0, 0, dimension.width, dimension.height);
		Swords.gameStateManager.render(graphics2D);
		Swords.mouseManager.render(graphics2D);
		clear();
	}
	
	/**
	 * Returns the current ticks per second.
	 * 
	 * @return The current ticks per second.
	 */
	public int getTPS() {
		return tps;
	}
	
	/**
	 * Returns the current frames per second.
	 * 
	 * @return The current frames per second.
	 */
	public int getFPS() {
		return fps;
	}
	
	/**
	 * ??????????????????????????????????????????????????????????????????????????????????
	 */
	public void clear() {
		Graphics g = getGraphics();
		if (image != null) {
			g.drawImage(image, 0, 0, null);
		}
		g.dispose();
	}
	
	/**
	 * ??????????????????????????????????????????????????????????????????????????????????????
	 * @param dimension ?????????????????????????????????????????????????????????????????????
	 */
	public void changeSize(Dimension dimension) {
		this.dimension = dimension;
		setPreferredSize(dimension);
		init();
	}
}
