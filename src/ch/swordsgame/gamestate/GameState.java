package ch.swordsgame.gamestate;

import java.awt.Graphics2D;

/**
 * The abstract class GameState, containing a gameStateManager.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public abstract class GameState {
	
	GameStateManager gameStateManager;
	
	public GameState(GameStateManager gameStateManager) {
		this.gameStateManager = gameStateManager;
	}

	/**
	 * Initialization.
	 */
	public abstract void init();
	
	/**
	 * Executes a tick.
	 * @param deltaTime The time which has passed since the last tick.
	 */
	public abstract void tick(double deltaTime);
	
	/**
	 * Executes render.
	 * @param g The graphics class, which should be used to render.
	 */
	public abstract void render(Graphics2D g);
	
	public GameStateManager getGameStateManager() {
		return gameStateManager;
	}
}
