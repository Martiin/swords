package ch.swordsgame.gamestate;

import java.awt.Graphics2D;
import java.util.Stack;

/**
 * The GameStateManager class. It handles the actions depending on the current gamestate.
 * 
 * @author Martin Bissiger	& Sergej Ilich
 *
 */
public class GameStateManager {
	
	/**
	 * A Stack containg all possible GameStates.
	 */
	public Stack<GameState> states;
	
	public GameStateManager() {
		states = new Stack<GameState>();
	}
	
	/**
	 * Executes one tick on the current state.
	 * 
	 * @param deltaTime The time, which has passed since the last tick.
	 */
	public void tick(double deltaTime) {
		states.peek().tick(deltaTime);
	}
	
	/**
	 * Renders a frame on the current state.
	 * 
	 * @param g The graphics class which should be used to render.
	 */
	public void render(Graphics2D g) {
		states.peek().render(g);
	}
	
	/**
	 * Executes a initialization on the current state.
	 */
	public void init() {
		states.peek().init();
	}
}
