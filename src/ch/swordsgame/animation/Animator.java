package ch.swordsgame.animation;

import java.awt.image.BufferedImage;
import java.util.ArrayList;

/**
 * The class which handles the animations.
 * 
 *  @author Martin Bissiger	& Sergej Ilich
 */
public class Animator {
	/**
	 * The sprite containing all images.
	 */
	public BufferedImage sprite;
	
	private ArrayList<BufferedImage> frames;
	private volatile boolean running = false;
	
	private long prevTime;
	private long speed;
	
	private int frameAtPause;
	private int currentFrame;
	
	/**
	 * Constructor
	 * 
	 * @param frames All frames
	 * 
	 */
	public Animator(ArrayList<BufferedImage> frames) {
		this.frames = frames;
	}
	
	/**
	 * Set a new animation speed.
	 * 
	 * @param speed The speed of the animation.
	 */
	public void setSpeed(long speed) {
		this.speed = speed;
	}
	
	/**
	 * Update the frames.
	 * 
	 * @param time ??????????????????????????????????????????????????????????????
	 */
	public void update(long time) {
		if (running) {
			if (time - prevTime >= speed) {
				currentFrame++;
				if (currentFrame < frames.size()) {
					sprite = frames.get(currentFrame);
				} else {
					currentFrame = 0;
					sprite = frames.get(currentFrame);
				}
				prevTime = time;
			}
		}
	}
	
	/**
	 * Run the animation.
	 */
	public void play() {
		prevTime = 0;
		frameAtPause = 0;
		currentFrame = 0;
		running = true;
	}
	
	/**
	 * Stop the animation.
	 */
	public void stop() {
		prevTime = 0;
		frameAtPause = 0;
		currentFrame = 0;
		running = false;
	}
	
	/**
	 * Pause the animation.
	 */
	public void pause() {
		frameAtPause = currentFrame;
		running = false;
	}
	
	/**
	 * Resume the animation.
	 */
	public void resume() {
		currentFrame = frameAtPause;
		running = true;
	}
	
	/**
	 * Reset the animation.
	 */
	public void reset() {
		currentFrame = 0;
	}
	
	/**
	 * Checks whether there are frames left to animate or not.
	 * 
	 * @return The boolean determining wheter there are frames left to animate or not.
	 */
	public boolean isDoneAnimating() {
		return currentFrame == frames.size();
	}
}
