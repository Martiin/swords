package ch.swordsgame.animation;

/**
 * An enum containing all animation states.
 * 
 *  @author Martin Bissiger	& Sergej Ilich
 *
 */
public enum AnimationState {
	UP,
	DOWN,
	LEFT,
	RIGHT,
	IDLE_UP,
	IDLE_LEFT,
	IDLE_DOWN,
	IDLE_RIGHT,
	FIGHT_UP,
	FIGHT_DOWN,
	FIGHT_LEFT,
	FIGHT_RIGHT,
	DEAD
}