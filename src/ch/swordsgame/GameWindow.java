package ch.swordsgame;

import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import javax.swing.JFrame;

/**
 * The game window which extends from JFrame.
 * 
 *  @author Martin Bissiger	& Sergej Ilich
 *
 */
public class GameWindow extends JFrame {
	boolean fullscreenEnabled = false;
	ScreenMode screenMode = ScreenMode.WINDOWED;
	GraphicsDevice device;
	
	/**
	 * The constructor with the title and screen as parameters.
	 * 
	 * @param title The title of the JFrame.
	 * @param screen ????????????????????????????????????????????????????????????????
	 */
	public GameWindow(String title, int screen) {
		init(title, screen);
	}
	
	/**
	 * The constructor with the title as paramter.
	 * 
	 * @param title The title of the JFrame.
	 */
	public GameWindow(String title) {
		init(title, 0);
	}
	
	/**
	 * ????????????????????????????????????????????????????????????????????????????????????
	 * @param screen ??????????????????????????????????????????????????????????????????????
	 */
	public void setScreen(int screen) {
		if (screen >= 0 && screen < GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices().length) {
			device = GraphicsEnvironment.getLocalGraphicsEnvironment().getScreenDevices()[screen];
			updateFullscreen();
		} else {
			System.err.println("Error Screen " + screen + " not found!");
		}
	}
	
	/**
	 * Sets the screen mode.
	 * @param mode The screen mode to apply on the application.
	 */
	public void setFullscreen(ScreenMode mode) {
		screenMode = mode;
		updateFullscreen();
	}
	
	private void init(String title, int screen) {
		setScreen(screen);
		setTitle(title);
		setLocationRelativeTo(null);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setResizable(false);
	}
	
	private void updateFullscreen() {
		switch (screenMode) {
		case WINDOWED:
			fullscreenEnabled = false;
			setUndecorated(false);
			updateSize();
			break;
		case BORDERLESS_WINDOW:
			fullscreenEnabled = false;
			setUndecorated(true);
			setExtendedState(JFrame.MAXIMIZED_BOTH);
			setLocation(device.getDefaultConfiguration().getBounds().x, device.getDefaultConfiguration().getBounds().y);
			updateSize();
			break;
		case FULLSCREEN:
			fullscreenEnabled = true;
			setUndecorated(true);
			device.setFullScreenWindow(this);
			updateSize();
			break;
		}
	}
	
	private void updateSize() {
		switch (screenMode) {
		case WINDOWED:
			setSize(device.getDisplayMode().getWidth() / 2, device.getDisplayMode().getHeight() / 2);
			break;
		case BORDERLESS_WINDOW:
		case FULLSCREEN:
			setSize(device.getDisplayMode().getWidth(), device.getDisplayMode().getHeight());
			break;

		default:
			break;
		}
	}
}